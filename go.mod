module gitlab.com/grassrootseconomics/cic-data-golang

go 1.16

require (
	github.com/btcsuite/btcd v0.22.0-beta // indirect
	github.com/bxcodec/faker/v3 v3.7.0 // indirect
	github.com/caarlos0/env/v6 v6.6.2
	github.com/emersion/go-vcard v0.0.0-20210521075357-3445b9171995
	github.com/ethereum/go-ethereum v1.10.15
	github.com/gin-gonic/gin v1.7.3
	github.com/go-co-op/gocron v1.7.0
	github.com/jackc/pgx/v4 v4.13.0 // indirect
	github.com/pkg/errors v0.9.1
	github.com/pkg/sftp v1.13.2
	github.com/shopspring/decimal v1.3.1 // indirect
	go.uber.org/zap v1.13.0
	golang.org/x/crypto v0.0.0-20220131195533-30dcbda58838
	golang.org/x/sys v0.0.0-20211216021012-1d35b9e2eb4e // indirect
	golang.org/x/text v0.3.7 // indirect
	gorm.io/driver/postgres v1.1.0
	gorm.io/gorm v1.21.13
)

replace gitlab.com/grassrootseconomics/cic-data-golang/common/cic/meta => ./common/cic/meta

replace gitlab.com/grassrootseconomics/cic-data-golang/common/restclient => ./common/restclient
