# CIC Data - Golang

## Getting Started

Setup the environmnet variables

```
cp .env_sample .env
source .env
```

```
docker-compose up -d
```
## Tests 

all unit tests
```
go test ./...
```

run integration tests via tags

list of tags: 

- sql (run against postgres db)

```
go test ./... -tags=sql
```
### environment variables explained

__postgres connection__

PG_HOST=localhost
PG_USER=analytics
PG_PASSWORD=fralala
PG_DB_NAME=cic_data
PG_PORT="5432"

__location of cic mapping files__

Notes these are baked into the image at this location. If you want to run without the contianer
then change to the relative path of ./common/cic/mapping/

USER_AREA_NAME_HOST="file://localhost/etc/cic/mappings/area_names.json"
USER_AREA_TYPE_HOST="file://localhost/etc/cic/mappings/area_types.json"
USER_PRODUCT_CATEGORY_HOST="file://localhost/etc/cic/mappings/product_categories.json"

__on-chain deployment parameters__

Get these from the cic deployment

CIC_REGISTRY_ADDRESS="CF60ebC445b636a5ab787F9E8BC465A2A3eF8299"
FAUCET_ADDRESS="103d1ed6e370dBa6267045c70d4999384c18a04A"
TRUST_ADDRESS="0xEb3907eCad74a0013c259D5874AE7f22DcBcC95C"

__These are configs for query of the cic-cache service to get relevant on-chain transactions__

Get the latest block number from the chain. This is just for the initial 'bootstrap' run. Subsequent
runs will get the end block number from the chain automatically.

BOOTSTRAPPING_END_BLOCK_NUMBER=10000000
UPDATE_TRANSACTION_BLOCK_DECREASE_NUMBER=5
UPDATE_TRANSACTION_BLOCK_INCREASE_NUMBER=100

__API endpoints to collect analytics__

RPC_PROVIDER: "http://rpc.sample.net"
META_SERVICE_URL: "http://meta.sample.net"
CACHE_SERVICE_URL: "http://cache.sample.net"

