# Dev Environment Setup Instructions

### Setting up `cic-data-golang` & `cic-internal-integration` on an Ubuntu 20.04 Digital Ocean Droplet

The main branch of `cic-internal-integration` is a fast moving target, so picking a stable commit and working on that for a few weeks is the best way to develop alongside of it. The aim will be to always use the latest commit to main once development work is stabilized and there is a a proper CI testing pipeline in place to ensure the main branch is stable.

These installation instructions are tested on a Linux server running Ubuntu 20.04. See the server setup instructions further down in this document.

## `cic-internal-integration` Setup

The monorepo needs to be running first and seeded with some data.

1. Git clone the repo
2. Checkout the commit that was the last known stable version that worked with cic-data
    ```bash
    git checkout ebf4743a
    ```
3. Bloxie setup (one time only)
    ```bash
    cd apps/bloxbergValidatorSetup
    docker build -t bloxie . && docker run -v "$(pwd)/keys:/root/keys" --rm -it -t bloxie account new --chain /root/bloxberg.json --keys-path /root/keys
    ```
4. Start building (using a tmux session)
    ```bash
    tmux
    cd apps/data-seeding
    rm -rf .venv
    python -m venv .venv
    source .venv/bin/activate
    python -m pip install --upgrade pip
    # pip install -r requirements.txt --extra-index-url https://pip.grassrootseconomics.net:8433 --extra-index-url https://pip.grassrootseconomics.net:8433 --index https://pypi.org/simple --pre
    # Added additional extra-index-url and no-cache per instruction from Philip
    pip install -r requirements.txt --extra-index-url https://pip.grassrootseconomics.net:8433 --extra-index-url https://pip.grassrootseconomics.net:8433 --index https://pypi.org/simple --pre --no-cache --extra-index-url https://gitlab.com/api/v4/projects/27624814/packages/pypi/simple
    rm -rf ~/out
    python create_import_users.py --gift-threshold 10000 --dir ~/out 10
    # in a new terminal in the root of the repo (no need to start the venv here)
    COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 docker-compose up --build -d
    # DEV_FAUCET_AMOUNT=50000000 RUN_MASK=3 COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 docker-compose up --build contract-migration
    # Added TOKEN_TYPE and TOKEN_SYMBOL per instruction from Blair
    TOKEN_TYPE=giftable_erc20_token TOKEN_SYMBOL=GFT DEV_FAUCET_AMOUNT=50000000 RUN_MASK=3 COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 docker-compose up --build contract-migration
    # wait for contract migration script to end before continuing, then start the remaining services
    COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 docker-compose up --build -d cic-cache-server cic-cache-tracker cic-cache-tasker cic-meta-server cic-user-ussd-server cic-user-tasker cic-user-server
    # flip back to the first terminal in the apps/data-seeding directory
    python cic_eth/import_balance.py -v -c config -p http://localhost:63545 -r 0xea6225212005e86a4490018ded4bf37f3e772161 -y ../contract-migration/keystore/UTC--2021-01-08T17-18-44.521011372Z--eb3907ecad74a0013c259d5874ae7f22dcbcc95c --head ~/out
    # create a second terminal in the apps/data-seeding directory (activate the venv here)
    python cic_eth/import_users.py -v -c config --redis-host-callback redis ~/out
    # Can also use later (>14.7.3 <15.0.0) LTS 14.x version
    nvm use 14.17.3
    npm install
    node cic_meta/import_meta.js ~/out 10
    node cic_meta/import_meta_phone.js ~/out 10
    node cic_meta/import_meta_custom.js ~/out 10
    # Verify everything worked - should see 0/10 for each item (except two skipped ussd items)
    python verify.py -v -c config -r 0xea6225212005e86a4490018ded4bf37f3e772161 -p http://localhost:63545 --token-symbol GFT --exclude ussd --exclude ussd_pins ~/ou1t
    # Optional: run the traffic script to generate transfers between addresses
    PYTHONPATH=./cic-eth/traffic:. python cic_eth/traffic/traffic.py -r 0xea6225212005e86a4490018ded4bf37f3e772161 -p http://localhost:63545 -c ~/gitlab/cic-internal-integration/apps/data-seeding/cic_eth/traffic/config -y ~/gitlab/cic-internal-integration/apps/contract-migration/keystore/UTC--2021-01-08T17-18-44.521011372Z--eb3907ecad74a0013c259d5874ae7f22dcbcc95c -vv --redis-host-callback redis --redis-port-callback 6379 -i evm:bloxberg:8996
    ```

## `cic-data-golang` Setup

1. Ensure `cic-internal-integration` is running correctly following the instructions in the section above
2. Go into the `cic-data-golang` directory and run it
   ```bash
   cd cic-data-golang
   make dev
   ```
3. Once everything is running, the bootstrapping job will start after 2 minutes and import all users and transactions into the DB
4. At this point you can then open Grafana to see the generated charts based on the DB data
5. To stop services open a new terminal and type:
   ```bash
   docker-compose down
   ```
6. To wipe the DBs type:
   ```bash
   docker volume rm cic-data-golang_grafana-storage cic-data-golang_postgres-db-data
   ```

## Basic Ubuntu Setup with Docker, Python, Node & Golang

1. Login to DO account/team and create a Basic droplet (Shared Regular Intel CPU, $20/mo, 4gb, 2cpu, 80gb, 4tb)
    - Image (Ubuntu 20.04 x64)
    - Datacenter Region (using AMS3)
    - Select additional options (only select Monitoring)
    - Choose a hostname
    - Create a password
2. Add the IP address for the new droplet to your local `/etc/hosts` file with its hostname (e.g., `xxx.xxx.xxx.xxx HOSTNAME`)
3. Login to the new droplet with SSH as root
    ```bash
    ssh root@[HOST]
    ```
4. Create the main user account for all of your future logins
    ```bash
    adduser [NAME]
    ```
5. Add the main user account to the sudo group
    ```bash
    gpasswd -a [NAME] sudo
    ```
6. On the command line enter `visudo` and change the following line (add `NOPASSWD: `), then ctrl-X/Y/Enter to save and exit
    ```bash
    %sudo   ALL=(ALL:ALL) NOPASSWD: ALL
    ```
7. Setup ssh directory
    ```bash
    mkdir .ssh && chmod 700 .ssh
    ```
8. Create public key store file and add your public ssh key (*make sure there are no hidden LFs when copying*)
    ```bash
    vi .ssh/authorized_keys
    ```
9. Set permissions on key store file
    ```bash
    chmod 600 .ssh/authorized_keys
    ```
10. Logout back to root user
    ```bash
    exit
    ```
11. Open a new terminal window and test login with main account (you don't need `[NAME]@` if you have the same username on your local machine as on the droplet)
    ```bash
    ssh [NAME]@[HOST]
    ```
12. Logout of root session in **other terminal window**
    ```bash
    exit
    ```
13. Test sudo access
    ```bash
    sudo vi /etc/ssh/sshd_config
    ```
14. While there, change `PermitRootLogin` from *yes* to *no* (and `PasswordAuthentication` from *yes* to *no* if you are using public key authentication)
15. In terminal window of main user account, restart ssh
    ```bash
    sudo service sshd restart
    ```
16. Check status of firewall (should be inactive)
    ```bash
    sudo ufw status
    ```
17. Add rules for SSH protocols
    ```bash
    sudo ufw allow 22/tcp
    sudo ufw show added
    ```
18. Enable firewall and show status
    ```bash
    sudo ufw enable
    sudo ufw status
    ```
19. Install zsh (logout and in again to effect changes)
    ```bash
    sudo apt-get update && sudo apt-get -y install zsh
    wget https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | zsh
    chsh -s `which zsh`
    ```
20. Logout and login again for zsh to kick in
21. Edit `.zshrc` and change `ZSH_THEME` to _**bira**_ so we always see user and host names in console (avoids a lot of stupid mistakes from happening)
    ```bash
    vi .zshrc
    ```
22. Update system and restart droplet
    ```bash
    sudo apt-get update && sudo apt-get -y dist-upgrade && sudo reboot
    ```
23. Setup Docker repo
    ```bash
    sudo apt-get update
    sudo apt-get install apt-transport-https ca-certificates curl gnupg lsb-release
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
    echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    ```
24. Install Docker Engine
    ```bash
    sudo apt-get update
    sudo apt-get install docker-ce docker-ce-cli containerd.io
    sudo docker run hello-world
    ```
25. Setup user in docker group to avoid starting every command with sudo
    ```bash
    sudo gpasswd -a $USER docker
    newgrp docker
    ```
26. Install Docker Compose
    ```bash
    sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose
    ```
27. Add aliases for docker and docker-compose
    ```bash
    vi .zshrc # then add =>
    alias d='docker'
    alias dc='docker-compose'
    alias dcleanc='docker rm -v $(docker ps -a -q -f status=exited)'
    alias dcleani='docker rmi -f $(docker images -f '\''dangling=true'\'' -q)'
    alias dcnuke='docker-compose down -v --rmi all --remove-orphans'
    alias di='docker images'
    alias dk='docker kill'
    alias dp='docker ps --format "table{{.ID}}\t{{.Names}}\t{{.Image}}\t{{.RunningFor}}\t{{.Status}}"'
    alias dpa='docker ps -a --format "table{{.ID}}\t{{.Names}}\t{{.Image}}\t{{.RunningFor}}\t{{.Status}}"'
    alias dv='docker volume'
    alias dvl='docker volume ls'
    alias dvr='docker volume rm'
    alias dvp='docker volume prune'
    ```
28. Setup Python alias
    ```bash
    sudo apt-get update && sudo apt-get install python-is-python3
    ```
29. Setup pyenv
    ```bash
    # https://realpython.com/intro-to-pyenv/#installing-pyenv
    sudo apt-get update; sudo apt-get install make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncursesw5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev
    curl https://pyenv.run | bash
    # Add these lines to the top of ~/.zshrc
    export PYENV_ROOT="$HOME/.pyenv"
    export PATH="$PYENV_ROOT/bin:$PATH"
    eval "$(pyenv init --path)"
    # Then restart the shell
    exec "$SHELL"
    pyenv --version
    ```
30. Install python 3.8.6 and make it the default
    ```bash
    pyenv install -v 3.8.6
    pyenv global 3.8.6
    ```
31. Setup nvm and make node 14 LTS the default
    ```bash
    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash
    # Add the following to ~/.zshrc
    export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
    [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
    # Then
    source ~/.zshrc
    nvm install 14.17.3
    ```
32. Install PostgreSQL drivers
    ```bash
    sudo apt-get install libpq-dev
    ```
33. Install Golang
    ```bash
    wget https://golang.org/dl/go1.16.7.linux-amd64.tar.gz
    sudo tar -C /usr/local -xzf go1.16.7.linux-amd64.tar.gz
    vi ~/.zshrc
    # Add this next line at the top of the file
    export PATH=$PATH:/usr/local/go/bin
    # Then instantiate the change
    source ~/.zshrc
    # And confirm it was installed
    go version
    ```

Now is a good time to shutdown the server and clone it so a new instance can be spun up quickly without having to repeat all of the above steps.

Once done, continue on to the CIC monorepo setup as described at the beginning of this document.
