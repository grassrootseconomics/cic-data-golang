# Transactions

## Types of Transactions

### Token shape 
- Same as ether? (18 zeros, wei, gwei, etc.?)
- Is one Sarafu == 1,000,000,000,000,000,000 wei?

Bergs == Bloxberg gas token

Normal gas token is a value transfer without input data (payload/input to the EVM)

Sarafu token == ERC20 token
methods: name, symbol, decimal (currently 6, internal calculations to about 30 decimals (quintillian interger amount of token plus left over decimals))

### P2P 
- User A pays User B X tokens (standard eth transaction, debit A and credit B addresses, gas fee (where is has deducted from))

### Contract-initiated

Redistribution done weekly
Anyone who does one transfer in that week gets a cut of collected demurrage

Application-initiated (no notion of scheduling/cronjob, external agent has to initiate them)
Do a transfer then check if time for distribution or demurrage

- Demurrage (2%/month negative interest rate calculated to the second)
  - How will demurrage fee be tracked and collected?
    - Maybe keep a running tally of the balance owed, 
    - Recalculate everytime the address' balance is changed
    - At month-end, calculate the remaining amount owed since last transaction and then deduct the total owed for the month
      - So if user is paid 100 tokens at the exact middle of the month and has no other transaction, at month end his address is deducted 1 token (2% of 100 but over half a month)
    - Is it possible to deduct funds from an address via a smart contract? IOW, how do we collect?

Call balanceOf function (standard ERC20) includes demurraged amount, plus non-standard call that gives non-demuragged balance

- Distributions (rewards for making payments)
  - What does a distribution transfer look like if it is not a standard P2P eth transfer?
  - How can these transactions (both distributions and demurrage) be recorded and shown to users in a traditional debit/credit account statement

- Gifting/Minting of new tokens (tokens given to users for signing up)
  - Where is this normalized into virtual transaction structures? How is it represented and where is it stored?
  - Not currently stored in CIC cache - can be translated into something for cache DB
  - Token has non-standard mint call (faucet contract on top of it to check for double-gifting, etc.)
  - cic-tracker-syncer has a callback plugin that looks for this transactions and sends it to USSD to confirm it has been done

## Accounting for Transactions

- Right now users have no real transaction history, i.e., a history of transactions they can search by date range, transaction type, etc.
- They just see their balance go up and down and reconcile that to SMS messages about ingoing/outgoing transactions.
- If one of the goals is to convince local gov'ts to accept tokens as payment of taxes, we need to ensure a complete accounting of transactions is in place so that transactions are auditable and can be imported into double-entry bookkeeping accounting systems (i.e., the requirement for a traditional debit/credit account statement)

##  Analytics

What proportion of users actually got redistributions for a period?
Who got redistributions at each period? 
  - not trivial to figure out over time because it is flattened each week or state storage would be huge) 
  - only thing stored is total redistribution amount per period, but no per-user data
  - need to recalculate separately active/passive account and do whole calculation over again to keep running record outside of blockchain
