# CIC Data Sequence Diagrams

The following sequence diagrams illustrate the flows and interactions between the `cic-data-*` services and their database tables along with the other `cic-*` services.

- Bootstrapping - initial loading of user (`meta`) and transaction (`cache`) data - migrated account data and opening balances
- Users
    - User additions - new users added to the system
    - User updates - changes to user data for existing users
- Transactions - the ongoing flow of transactions recorded in the blockchain
- Reporting 
    - Grafana - transaction data for charting
    - CSV data - user data and processed transaction statistics

## Bootstrapping

```mermaid
sequenceDiagram

  participant cdt as CicDataTasker
  participant ut as UsersTable
  participant tt as TransactionsTable
  participant cms as CicMetaServer
  participant ccs as CicCacheServer

  rect rgb(100, 100, 100, .5)
  cdt->>+cms: Fetch all imported users
  cms-->>-cdt: Return all users
  Note left of cms: Migrated users
  cdt->>cdt: Process user records
  loop Each user
  cdt->>+ut: Insert user data
  ut->>-cdt: Insert operation confirmed
  end
  end
  rect rgb(150, 150, 150, .75)
  cdt->>+ccs: Fetch all transactions
  ccs-->>-cdt: Return all transactions
  Note left of ccs: Opening balances
  cdt->>cdt: Process transactions
  loop Each user
  cdt->>+tt: Insert transaction data
  cdt->>+ut: Update user balance
  tt->>-cdt: Insert operation confirmed
  ut->>-cdt: Update operation confirmed
  end
  end
```

## User Additions & Updates

```mermaid
sequenceDiagram
  participant cus as CicUserServer
  participant cds as CicDataServer
  participant ut as UsersTable
  participant cdt as CicDataTasker
  participant cms as CicMetaServer

  loop Ongoing
  cus-->>cds: Inform new/changed user record
  Note right of cus: Address: 0xf24c...
  cds->>ut: Set `pending_update` for 0xf24c
  cus-->>cds: Inform new/changed user record
  Note right of cus: Address: 0x39a1...
  cds->>ut: Set `pending_update` for 0x39a1
  end

  loop Every minute
  cdt->>+ut: Fetch `pending_update` user records
  ut->>-cdt: Return list of records
  Note right of ut: [0xf24c, 0x39a1]
  cdt->>+cms: Fetch new/changed records
  cms-->>-cdt: Return records
  Note left of cms: [{ add: 0x39a1 }, {add: 0xf24c}]
  cdt->>cdt: Process new/changed records
  cdt->>+ut: Upsert new/changed records
  ut->>-cdt: Upsert operation confirmed
  Note right of ut: [{ add: 0x39a1, ... }, {add: 0xf24c, ...}]
  end
```

## Transactions

```mermaid
sequenceDiagram
  participant cdt as CicDataTasker
  participant tt as TransactionsTable
  participant ut as UsersTable
  participant ccs as CicCacheServer

  loop Every minute
    rect rgb(100, 100, 100, .5)
    cdt->>+tt: Fetch block number of last block processed
    tt->>-cdt: Return last block
    Note left of tt: Block 227
    cdt->>+ccs: Fetch block 228 and up
    ccs-->>-cdt: Return blocks and their transactions
    Note left of ccs: Blocks 228-233
    end

    rect rgb(150, 150, 150, .75)
    cdt->>cdt: Process new blocks
    cdt->>+ut: Request user data based on blockchain address
    ut->>-cdt: Return user data
    cdt->>cdt: Create new transactions
    cdt->>+tt: Insert new transaction data
    tt->>-cdt: Insert operation confirmed
    Note left of tt: Transactions in blocks 228-233
    cdt->>+ut: Update user stats based on new transactions
    ut->>-cdt: Update operation confirmed
    Note left of ut: User A total txns = Y, etc.
    end
  end
```

## Reporting

### Grafana Charts

```mermaid
sequenceDiagram

  participant usr as User
  participant grf as Grafana
  participant tt as TransactionsTable

  usr->>+grf: Request chart (params)
  grf->>+tt: Get transaction data (SQL query)
  tt->>-grf: Return data as SQL
  grf->>grf: Generate chart
  grf->>-usr: Display chart
```

### CSV Data

Spreadsheet of user statistics can be pulled from `cic-data-server` through an authenticated API call from CICADA to ensure only admins have access to private data.

```mermaid
sequenceDiagram

  participant adm as Admin
  participant cds as CicDataServer
  participant ut as UsersTable

  adm->>+cds: Request CSV file
  cds->>+ut: Get user data (SQL query)
  ut->>-cds: Return data as SQL
  cds->>cds: Generate CSV file
  cds->>-adm: Return CSV file
```
