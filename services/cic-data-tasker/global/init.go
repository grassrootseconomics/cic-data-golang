package global

import (
	"log"
	"os"
	"sync"

	"gitlab.com/grassrootseconomics/cic-data-golang/common/pg"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/config"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/internal/job"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/migrations"
)

var once sync.Once

func Init() {
	once.Do(func() {
		config.Init()
		log.Printf("environment configuration: \n%+v", config.Conf)
		pg.Init(&pg.Config{
			HOST:     config.Conf.PG.HOST,
			USER:     config.Conf.PG.USER,
			PASSWORD: config.Conf.PG.PASSWORD,
			DBName:   config.Conf.PG.DBName,
			PORT:     config.Conf.PG.PORT,
		})
		migrations.Migrate()
		if config.Conf.GenerateFakeData == "true" {
			log.Println("Generating fake data for the frontend")
			migrations.GenerateFakeData()
			log.Println("Flinished generating fake data. \nExiting.")
			os.Exit(0)
		}
		job.Init()
	})
}
