package main

import (
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/internal/app"
)

func main() {
	app.StartApplication()
}
