package config

import (
	"log"
	"time"

	"github.com/caarlos0/env/v6"
)

var Conf = config{}

type config struct {
	Server           serverConf
	PG               pgConf
	UserResource     userResourceConf
	SystemAddress    systemAddressConf
	BlockNumber      blockNumberConf
	DataProvider     dataProviderConf
	Web3Provider     web3Conf
	MetaServiceUrl   string `env:"META_SERVICE_URL"`
	CacheServiceUrl  string `env:"CACHE_SERVICE_URL"`
	Environment      string `env:"ENVIRONMENT"`
	GenerateFakeData string `env:"GENERATE_FAKE_DATA"`
}

type serverConf struct {
	Port         string        `env:"SERVER_PORT"`
	TimeoutRead  time.Duration `env:"SERVER_TIMEOUT_READ"`
	TimeoutWrite time.Duration `env:"SERVER_TIMEOUT_WRITE"`
	TimeoutIdle  time.Duration `env:"SERVER_TIMEOUT_IDLE"`
}

type pgConf struct {
	USER     string `env:"PG_USER"`
	PASSWORD string `env:"PG_PASSWORD"`
	HOST     string `env:"PG_HOST"`
	DBName   string `env:"PG_DB_NAME"`
	PORT     string `env:"PG_PORT"`
}

type userResourceConf struct {
	AreaNameHost        string `env:"USER_AREA_NAME_HOST"`
	AreaTypeHost        string `env:"USER_AREA_TYPE_HOST"`
	ProductCategoryHost string `env:"USER_PRODUCT_CATEGORY_HOST"`
}

type systemAddressConf struct {
	TrustAddress string `env:"TRUST_ADDRESS"`
}

type blockNumberConf struct {
	BootstrappingEnd          int `env:"BOOTSTRAPPING_END_BLOCK_NUMBER:"`
	UpdateTransactionDecrease int `env:"UPDATE_TRANSACTION_BLOCK_DECREASE_NUMBER"`
	UpdateTransactionIncrease int `env:"UPDATE_TRANSACTION_BLOCK_INCREASE_NUMBER"`
}

// TODO remove UserHost and UserPort moved to MetaServiceUrl
type dataProviderConf struct {
	TransactionHost string `env:"DATA_PROVIDER_TRANSACTIONS_HOST"`
	TransactionPort string `env:"DATA_PROVIDER_TRANSACTIONS_PORT"`
	UserHost        string `env:"DATA_PROVIDER_USERS_HOST"`
	UserPort        string `env:"DATA_PROVIDER_USERS_PORT"`
}

type web3Conf struct {
	RpcProvider     string `env:"RPC_PROVIDER"`
	RegistryAddress string `env:"CIC_REGISTRY_ADDRESS"`
}

func Init() {
	err := env.Parse(&Conf)
	if err != nil {
		log.Fatalf("Failed to decode environment variables: %s", err)
	}
}
