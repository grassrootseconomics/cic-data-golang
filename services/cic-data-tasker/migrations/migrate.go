package migrations

import (
	"log"
	"math/rand"

	"github.com/bxcodec/faker/v3"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/pg"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/util/testutils"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/config"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/internal/dao"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/internal/domain"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/internal/testutils/mocks"
)

func init() {
	config.Init()

	pg.Init(&pg.Config{
		HOST:     config.Conf.PG.HOST,
		USER:     config.Conf.PG.USER,
		PASSWORD: config.Conf.PG.PASSWORD,
		DBName:   config.Conf.PG.DBName,
		PORT:     config.Conf.PG.PORT,
	})
}

func Migrate() {
	db := pg.Client()

	err := db.AutoMigrate(&domain.UserEntity{},
		&domain.TokenEntity{},
		&domain.BalanceEntity{},
		&domain.TransactionEntity{})

	if err != nil {
		log.Panicf("✋ hol up, can't migrate schemas: %s", err)
	}
}

func GenerateFakeData() {
	db := pg.Client()

	// just another safeguard to running this in the wrong environment
	if config.Conf.Environment != "DEV" {
		log.Println("I'm sorry we can only run createFakeData in a development environment")
		return
	}
	db.Migrator().DropTable(&domain.UserEntity{}, &domain.TokenEntity{}, &domain.BalanceEntity{}, &domain.TransactionEntity{})
	err := db.AutoMigrate(&domain.UserEntity{},
		&domain.TokenEntity{},
		&domain.BalanceEntity{},
		&domain.TransactionEntity{})
	if err != nil {
		log.Panicf("✋ hol up, can't migrate schemas: %s", err)
	}

	mocks.CustomGenerator()
	tokenPG := dao.TokenPG{}
	for i := 1; i < 10; i++ {
		var token domain.TokenEntity
		err = faker.FakeData(&token)
		if err != nil {
			log.Printf("error in fake token creation %s", err)
		}
		log.Printf("fake token \n %+v", token.TokenAddress)
		err := tokenPG.AddToken(db, &token)
		if err != nil {
			log.Printf("failed to add fake token %s", err)
		}
	}

	userPG := dao.UserPG{}
	transactionPG := dao.TransactionPG{}
	var previousAddress string = testutils.GenerateEthAddress().String()

	tokenEntities, err := tokenPG.GetAllTokens(db)
	if err != nil {
		log.Printf("could not get tokens %s", err)
	}

	numTokens := len(tokenEntities)
	log.Printf("found %d tokens", len(tokenEntities))
	for j := 1; j < 1000; j++ {
		var user domain.UserEntity
		err = faker.FakeData(&user)
		if err != nil {
			log.Printf("error in fake user creation %s", err)
		}
		userPG.AddUser(db, &user)

		//add transactions
		var transaction domain.TransactionEntity
		err = faker.FakeData(&transaction)
		if err != nil {
			log.Printf("error in fake transaction creation %s", err)
		}

		transaction.SenderAddress = user.BlockchainAddress
		//log.Printf("transaction address %s", transaction.SenderAddress)
		transaction.ReceiverAddress = previousAddress
		thisToken := tokenEntities[rand.Intn(numTokens)].TokenAddress
		transaction.SenderToken = thisToken[2:]
		transaction.ReceiverToken = thisToken[2:]
		transaction.AmountReceived = transaction.AmountSent
		log.Printf("adding txn %s", transaction.AmountReceived.String())
		transactionPG.AddTransaction(db, transaction)

		previousAddress = user.BlockchainAddress
	}

}
