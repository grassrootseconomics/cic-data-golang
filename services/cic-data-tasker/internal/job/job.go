package job

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/big"
	"net/http"
	"time"

	"github.com/ethereum/go-ethereum/common"
	"github.com/pkg/errors"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/bigint"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/cic/meta"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/httputil"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/pg"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/server"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/web3"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/config"
	"gorm.io/gorm"

	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/internal/dao"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/internal/domain"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/internal/service"

	"github.com/go-co-op/gocron"
)

var (
	userSvc        *service.UserService
	tokenSvc       *service.TokenService
	transactionSvc *service.TransactionService
	s              *gocron.Scheduler
	web3Util       *web3.Web3
	db             *gorm.DB
	web3Conf       *web3.Conf
	metaClient     *meta.MetaClient
)

func Init() {
	var err error

	web3Util, err = web3.NewWeb3(httputil.NewDefaultClient(), &web3.Conf{
		RpcProvider:     config.Conf.Web3Provider.RpcProvider,
		MetaHost:        config.Conf.DataProvider.UserPort,
		MetaPort:        config.Conf.DataProvider.UserHost,
		RegistryAddress: config.Conf.Web3Provider.RegistryAddress,
	})
	if err != nil {
		log.Fatalf("cannot connect to web3: %v", err)
	}
	logger, err := server.NewZapLogger()
	if err != nil {
		log.Fatalf("cannot create logger: %v \n", err)
	}

	userSvc = &service.UserService{
		PG:     &dao.UserPG{},
		Web3:   web3Util,
		Logger: logger,
	}
	transactionSvc = &service.TransactionService{
		UserPG:        &dao.UserPG{},
		TransactionPG: &dao.TransactionPG{},
		Web3:          web3Util,
		Logger:        logger,
	}
	tokenSvc = &service.TokenService{
		Web3:    web3Util,
		Logger:  logger,
		TokenPG: &dao.TokenPG{},
	}
	db = pg.Client()
	metaClient, _ = meta.NewMetaClient(config.Conf.MetaServiceUrl)

	jobFindNewTokensAndUsersToAdd()
	jobUpdateTransactions()
	activateJobs()
}

func activateJobs() {
	s = gocron.NewScheduler(time.UTC)
	s.Every(10).Minute().SingletonMode().Do(jobFindNewTokensAndUsersToAdd)
	s.Every(10).Minute().SingletonMode().Do(jobUpdateBalances)
	s.Every(10).Minute().SingletonMode().Do(jobUpdateTransactions)
	s.StartAsync()
}

func jobFindNewTokensAndUsersToAdd() {

	newTokens, err := getNewTokensToAdd()
	if err != nil {
		log.Printf("error getting tokens to add %e", err)
	}

	log.Printf("New tokens to add %+v", len(newTokens))
	err = addTokenDetailsToTokens(newTokens)
	if err != nil {
		log.Printf("could not get new token details %s", err)
	}

	err = commitTokensToDb(newTokens)
	if err != nil {
		log.Printf("could not put new tokens in database %s", err)
	}

	newUsers, err := getNewUsersToAdd()
	if err != nil {
		log.Printf("error during boostrapping %s", err)
	}
	if len(newUsers) == 0 {
		log.Println("No new users to add")
		return
	}
	log.Printf("New users to add %+v", len(newUsers))

	err = addMetaPersonDataToUser(newUsers)
	if err != nil {
		log.Printf("error adding person meta data to users %s", err)
	}

	err = addBalancesToUsers(newUsers)
	if err != nil {
		log.Printf("error getting balances for users %s", err)
	}

	err = addMetaCustomDataToUsers(newUsers)
	if err != nil {
		log.Printf("error getting account type for users %s", err)
	}

	// TODO add check if user is sufficiently populated to commit to db

	err = commitUsersToDb(newUsers)
	if err != nil {
		log.Printf("job commitUsersToDb error %s", err)
	}
}

func getNewTokensToAdd() ([]*domain.TokenEntity, error) {
	allAddresses, err := tokenSvc.GetAllTokenAddresses()
	if err != nil {
		log.Printf("Job --> updateToken cannot get token address: %s", err)
	}
	newTokens := make([]*domain.TokenEntity, 0)
	for _, tokenAddress := range allAddresses {
		exists, err := tokenSvc.TokenPG.TokenExists(db, tokenAddress.String())
		if err != nil {
			log.Printf("could not check if token exists %s", err)
		}
		if !*exists {
			newTokens = append(newTokens, &domain.TokenEntity{TokenAddress: tokenAddress.String()})
		}
	}
	return newTokens, nil
}

func commitTokensToDb(tokens []*domain.TokenEntity) error {
	for _, token := range tokens {
		err := tokenSvc.TokenPG.AddToken(db, token)
		if err != nil {
			log.Printf("could not add token to database %s", err)
		}
	}
	log.Printf("added %d tokens to the database!", len(tokens))
	return nil
}

// calls the Account Registry contract to get all of the users and then checks if they are in the database
// returns a user db object for each user not in the database
// TODO this could be optimized by using the entry id from the contract to keep a pointer to latest added user
func getNewUsersToAdd() ([]*domain.UserEntity, error) {
	log.Println("Fetching all account addresses from Account Registry this may take a while...")
	addresses, err := web3Util.GetAllAddrs()
	if err != nil {
		return nil, err
	}

	var newAddresses []*common.Address

	for _, address := range addresses {
		exists, err := userSvc.PG.UserExists(db, address.String())
		if err != nil {
			return nil, err
		}
		if !*exists {
			newAddresses = append(newAddresses, address)
		}
	}

	log.Printf("Count of new users in Account Registry to add: %d", len(newAddresses))
	var newUsers []*domain.UserEntity
	for _, address := range newAddresses {
		newUsers = append(newUsers, domain.NewUser(address))
	}

	return newUsers, nil
}

func addTokenDetailsToTokens(tokens []*domain.TokenEntity) error {
	for _, token := range tokens {
		tokenCommonAddress := common.HexToAddress(token.TokenAddress)
		tokenDeets, err := web3Util.GetTokenDetails(&tokenCommonAddress)
		if err != nil {
			log.Println("could not get token details for new token")
		}
		token.Name = tokenDeets.Name
		token.Symbol = tokenDeets.Symbol
	}
	return nil
}

// get meta keys person and custom and merge with UserEntity
func addMetaPersonDataToUser(users []*domain.UserEntity) error {
	for _, user := range users {
		userAddress := common.HexToAddress(user.BlockchainAddress)
		res, err := metaClient.GetMetaPerson(&userAddress, http.Header{})
		if err != nil {
			log.Printf("unnable to get person data for user Status %d Error Message %s", err.StatusCode, err.Message)
			continue
		}
		domain.MergeMetaResponsePerson(user, res)
	}
	return nil
}

// get meta keys account type and merge with user entity
func addMetaCustomDataToUsers(users []*domain.UserEntity) error {
	for _, user := range users {
		userAddress := common.HexToAddress(user.BlockchainAddress)
		res, err := metaClient.GetMetaCustom(&userAddress, http.Header{})
		if err != nil {
			log.Printf("unnable to get custom data for user: %s", err.Message)
		}
		domain.MergeMetaResponseCustom(user, res)
	}
	return nil
}

func addMetaPreferencesDataToUsers(users []*domain.UserEntity) error {
	for _, user := range users {
		userAddress := common.HexToAddress(user.BlockchainAddress)
		res, err := metaClient.GetMetaPreferences(&userAddress, http.Header{})
		if err != nil {
			log.Printf("unnable to get custom data for user: %s", err.Message)
		}
		domain.MergeMetaResponsePreferences(user, res)
	}
	return nil
}

// for each token in the database get the user balance ADD it to the tokenBalance and return the user db object
func addBalancesToUsers(users []*domain.UserEntity) error {
	tokens, err := tokenSvc.TokenPG.GetAllTokens(db)
	if err != nil {
		return err
	}
	for _, user := range users {
		for _, token := range tokens {
			userAddress := common.HexToAddress(user.BlockchainAddress)
			tokenAddress := common.HexToAddress(token.TokenAddress)
			balance, err := web3Util.GetUserTokenBalance(userAddress, tokenAddress)
			if err != nil {
				log.Printf("could not get token balance for user: %s", err)
				continue
			}
			if balance.Cmp(big.NewInt(0)) > 0 {
				newBalanceEntity := domain.BalanceEntity{
					TokenAddress: tokenAddress.String(),
					Amount:       bigint.NewBigint(balance),
				}
				user.TokenBalances = append(user.TokenBalances, newBalanceEntity)
			}
		}
	}
	return nil
}

func commitUsersToDb(users []*domain.UserEntity) error {
	for _, user := range users {
		err := userSvc.PG.AddUser(db, user)
		if err != nil {
			return err
		}
	}
	return nil
}

func jobUpdateTransactions() {
	log.Println("Updating Transactions...")

	err := userSvc.UpdatePendingUser()
	if err != nil {
		log.Println(errors.Errorf("Job -> updateTransactions Error -> %s", err))
		return
	}

	latestBlockNum, err := transactionSvc.SelectHighestBlock()
	if err != nil {
		log.Println(errors.Errorf("Job -> updateTransactions Error -> %s", err))
		return
	}

	updateTransactionDecrease := config.Conf.BlockNumber.UpdateTransactionDecrease

	startBlock := latestBlockNum

	if latestBlockNum > updateTransactionDecrease {
		startBlock = latestBlockNum - config.Conf.BlockNumber.UpdateTransactionDecrease
	}

	endBlock := latestBlockNum + config.Conf.BlockNumber.UpdateTransactionIncrease

	log.Printf("get transaction from %v to %v\n", startBlock, endBlock)

	transactionCollections, err := getTransactionCollections(startBlock, endBlock)
	log.Printf("number of transactions to add %d", len(transactionCollections))
	if err != nil {
		log.Println(errors.Errorf("Job -> updateTransactions Error -> %s", err))
		return
	}

	// pass data to service
	err = transactionSvc.ProcessAllTransaction(transactionCollections)
	if err != nil {
		log.Println(errors.Errorf("Job -> updateTransactions Error -> %s", err))
		return
	}

	log.Println("Update Transactions Done!")
}

func getTransactionCollections(startBlock int, endBlock int) (domain.TransactionCollections, error) {
	url := fmt.Sprintf("%s/txa/0/0/%d/%d", config.Conf.CacheServiceUrl, startBlock, endBlock)

	client := http.Client{
		Timeout: time.Second * 10,
	}
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, errors.Errorf("getTransactionCollections Error -> %s", err)
	}
	req.Header.Set("X-CIC-CACHE-MODE", "all")
	res, err := client.Do(req)
	if err != nil {
		return nil, errors.Errorf("getTransactionCollections Error -> %s", err)
	}

	if res.StatusCode != 200 {
		return nil, errors.Errorf("getTransactionCollections Error -> Request Transactions Data Error %s", res.Status)
	}

	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, errors.Errorf("getTransactionCollections Error -> %s", err)
	}

	var transactionGetData domain.TransactionGetData

	err = json.Unmarshal(body, &transactionGetData)
	if err != nil {
		return nil, errors.Errorf("getTransactionCollections Error -> %s", err)
	}

	return transactionGetData.TransactionCollections, nil
}

// find all transactions in last runIntervalMinutes minutes and update balance for all sender and receivers
func jobUpdateBalances(runIntervalMinutes int) error {
	// TODO hard to test this function, break into 1) get recent transactions 2) get applicable token addresses (those invovled in transactions)
	// 3) update balances as a function of 1 and 2
	if runIntervalMinutes < 0 {
		return errors.New("runInternalMinutes must be some positive integer")
	}
	now := time.Now()
	from := now.Add(-(time.Minute * time.Duration(runIntervalMinutes)))
	transactions, err := transactionSvc.TransactionPG.FindTransactionInTimeSlice(db, from, now)
	if err != nil {
		return err
	}

	// get a set of unique addresses to lookup
	addressLookups := map[string]bool{}
	for _, transaction := range transactions {
		_, ok := addressLookups[transaction.ReceiverAddress]
		if !ok {
			addressLookups[transaction.ReceiverAddress] = true
		}
		_, ok = addressLookups[transaction.SenderAddress]
		if !ok {
			addressLookups[transaction.SenderAddress] = true
		}
	}

	// this is a pretty rough approach to keeping balances synced. Using the balanceOf method on the contract
	// as the source of truth, we call that method if user or sender was involved in a transaction in the last
	// runInternalMinutes period. Setting new balance entries for the user and replacing the old ones
	tokens, err := tokenSvc.TokenPG.GetAllTokens(db)
	if err != nil {
		log.Printf("could not get tokens from database in update balance %s", err)
	}

	for userAddressString, _ := range addressLookups {
		userAddress := common.HexToAddress(userAddressString)
		for _, token := range tokens {
			tokenAddress := common.HexToAddress(token.TokenAddress)
			amount, err := web3Util.GetUserTokenBalance(userAddress, tokenAddress)
			if err != nil {
				log.Printf("could not get balance for user in update balances %s", err)
			}
			err = userSvc.PG.UpdateUserTokenBalance(db, userAddress.String(), tokenAddress.String(), bigint.NewBigint(amount))
			if err != nil {
				log.Printf("could not update users token balance %s", err)
			}
		}
	}
	return nil
}
