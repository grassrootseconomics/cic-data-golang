package dao

import (
	"fmt"
	"log"

	"github.com/pkg/errors"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/internal/domain"
	"gorm.io/gorm"
)

type TokenPG struct{}

// Add a single token
func (t *TokenPG) AddToken(db *gorm.DB, token *domain.TokenEntity) error {
	err := db.Create(token).Error
	if err != nil {
		return errors.Errorf("Repository/Token -> AddToken Error -> %s", err)
	}
	return nil
}

// Get all tokens
func (t *TokenPG) GetAllTokens(db *gorm.DB) ([]*domain.TokenEntity, error) {
	tokens := []*domain.TokenEntity{}
	err := db.Find(&tokens).Error
	if err != nil {
		return nil, err
	}
	return tokens, nil
}

// Check if token with blockchain address exists
func (u *TokenPG) TokenExists(db *gorm.DB, tokenBlockchainAddress string) (*bool, error) {
	var exists bool
	query := fmt.Sprintf("SELECT EXISTS (SELECT id FROM tokens WHERE token_address='%s')", tokenBlockchainAddress)
	result := db.Raw(query).Scan(&exists)
	if result.Error != nil && !errors.Is(result.Error, gorm.ErrRecordNotFound) {
		log.Printf("error checking if user exists %s", result.Error)
		return nil, result.Error
	}
	return &exists, nil
}
