package dao

import (
	"fmt"
	"log"
	"math/big"
	"time"

	"github.com/pkg/errors"

	"gitlab.com/grassrootseconomics/cic-data-golang/common/bigint"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/web3"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/config"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/internal/domain"
	"gorm.io/gorm"
)

// TODO consider removing the struct because there is no state and these dao functions can be statics
// TODO the pointer receiver can just be the UserEntity
// Add a single user. Balances are auto inserted.
type UserPG struct{}

var (
	ErrUserNotFound = errors.New("User not found")
)

// Add a single user. Balances are auto inserted.
func (u *UserPG) AddUser(db *gorm.DB, user *domain.UserEntity) error {
	result := db.Create(&user)
	if result.Error != nil {
		return result.Error
	}
	db.Save(&user)
	return nil
}

// Gets a user from the database without requiring a web3 dependency
func (u *UserPG) GetUserByAddress(db *gorm.DB, blockchainAddress string) (*domain.UserEntity, error) {
	user := &domain.UserEntity{}
	result := db.Where("blockchain_address = ?", blockchainAddress).Find(user)
	if result.Error != nil && errors.Is(result.Error, gorm.ErrRecordNotFound) {
		return nil, ErrUserNotFound
	}
	if result.Error != nil {
		log.Printf("a error occurred while looking up a user %s", result.Error)
		return nil, result.Error
	}
	return user, nil
}

// Gets user from database and also filter for faucet contract
func (u *UserPG) FindUserByBlockchainAddress(db *gorm.DB, blockchainAddress string, w *web3.Web3) (*domain.UserEntity, error) {
	var user domain.UserEntity
	trustAddress := config.Conf.SystemAddress.TrustAddress
	prefixAddress := fmt.Sprintf("0x%s", blockchainAddress)
	//faucetAddress := config.Conf.SystemAddress.FaucetAddress

	faucetCommonAddress, err := w.GetContractAddress("Faucet")
	if err != nil {
		fmt.Errorf("faucet contract cannot be found")
	}
	faucetAddress := faucetCommonAddress.String()

	if prefixAddress == trustAddress || prefixAddress == faucetAddress {
		user = domain.UserEntity{
			BlockchainAddress: "SYSTEM_ADDRESS",
			DateRegistered:    time.Date(1970, time.Month(1), 1, 0, 0, 0, 0, time.UTC),
			Age:               0,
			ProductCategory:   "system",
			AreaType:          "system",
			AreaName:          "system",
			Gender:            "system",
			OvolIn: bigint.NewBigint(big.NewInt(0)),
			OvolOut: bigint.NewBigint(big.NewInt(0)),
			SvolIn: bigint.NewBigint(big.NewInt(0)),
			SvolOut: bigint.NewBigint(big.NewInt(0)),
		}
	} else {
		if err := db.Raw("SELECT * FROM users WHERE blockchain_address = ? ORDER BY id", prefixAddress).Scan(&user).Error; err != nil {
			return nil, errors.Errorf("Repository/User -> FindUserByBlockchainAddress Error -> %s", err)
		}
	}

	return &user, nil
}

func (u *UserPG) UpdateUser(db *gorm.DB, user domain.UserEntity) error {
	if err := db.Exec(
		"UPDATE users SET "+
			"date_registered = @DateRegistered,"+
			"days_enrolled = @DaysEnrolled,"+
			"product_category = @ProductCategory,"+
			"gender = @Gender,"+
			"age = @Age,"+
			"area_name = @AreaName,"+
			"area_type = @AreaType,"+
			"updated_at = @UpdatedAt,"+
			"account_type = @AccountType,"+
			"preferred_language = @PreferredLanguage,"+
			"is_market_enabled = @IsMarketEnabled,"+
			"pending_update = @PendingUpdate,"+
			//"token_balance = @TokenBalance,"+
			"ovol_in = @OvolIn,"+
			"ovol_out = @OvolOut,"+
			"otxns_in = @OtxnsIn,"+
			"otxns_out = @OtxnsOut,"+
			"ounique_in = @OuniqueIn,"+
			"ounique_out = @OuniqueOut,"+
			"svol_in = @SvolIn,"+
			"svol_out = @SvolOut,"+
			"stxns_in = @StxnsIn,"+
			"stxns_out = @StxnsOut,"+
			"sunique_in = @SuniqueIn,"+
			"sunique_out = @SuniqueOut,"+
			"sunique_all = @SuniqueAll,"+
			"last_trade_out = @LastTradeOut,"+
			"first_trade_in_user = @FirstTradeInUser,"+
			"first_trade_in_time = @FirstTradeInTime "+
			"WHERE blockchain_address = @BlockchainAddress",
		user).Error; err != nil {
		return errors.Errorf("Repository/User -> UpdateUser Error -> %s", err)
	}
	return nil
}

func (u *UserPG) GetAllUser(db *gorm.DB) ([]*domain.UserEntity, error) {
	var users domain.Users
	if err := db.Raw("SELECT * FROM users ORDER BY id").Scan(&users).Error; err != nil {
		return nil, errors.Errorf("Repository/User -> GetAllUser Error -> %s", err)
	}

	return users, nil
}

func (u *UserPG) FindPendingUser(db *gorm.DB) ([]*domain.UserEntity, error) {
	var users domain.Users
	if err := db.Raw("SELECT * FROM users WHERE pending_update = true ORDER BY id").Scan(&users).Error; err != nil {
		return nil, errors.Errorf("Repository/User -> FindPendingUser Error -> %s", err)
	}

	return users, nil
}

// Updates a single user balance for a token
func (u *UserPG) UpdateUserTokenBalance(db *gorm.DB, userBlockchainAddress string, tokenAddress string, amount *bigint.Bigint) error {
	err := db.Exec(
		"UPDATE balances "+
			"SET amount = ? "+
			"WHERE balances.user_id = "+
			"(select (users.id) from users WHERE users.blockchain_address = ?) "+
			"AND balances.token_address = ?",
		amount,
		userBlockchainAddress, //The UserEntity is using a string type address
		tokenAddress,
	).Error
	if err != nil {
		return err
	}
	return nil
}

// Get a single user token balance
func (u *UserPG) GetUserTokenBalance(db *gorm.DB, userBlockchainAddress string, tokenAddress string) (*domain.BalanceEntity, error) {
	balance := domain.BalanceEntity{}
	err := db.First(&balance).Exec(`SELECT * FROM balances WHERE user_id = (
    SELECT id FROM users WHERE blockchain_address = ?)
		AND token_address = ?`,
		userBlockchainAddress,
		tokenAddress).Error
	if err != nil {
		return nil, err
	}
	return &balance, nil
}

// Check if user with blockchain address exists
func (u *UserPG) UserExists(db *gorm.DB, userBlockchainAddress string) (*bool, error) {
	var exists bool
	query := fmt.Sprintf("SELECT EXISTS (SELECT id FROM users WHERE blockchain_address='%s')", userBlockchainAddress)
	result := db.Raw(query).Scan(&exists)
	if result.Error != nil && !errors.Is(result.Error, gorm.ErrRecordNotFound) {
		log.Printf("error checking if user exists %s", result.Error)
		return nil, result.Error
	}
	return &exists, nil
}
