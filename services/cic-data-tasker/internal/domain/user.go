package domain

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"math"
	"math/big"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"time"

	"github.com/ethereum/go-ethereum/common"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/bigint"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/cic/meta"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/util"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/config"

	"gitlab.com/grassrootseconomics/cic-data-golang/common/date"
)

type UserEntity struct {
	ID                int `gorm:"column:id;primaryKey" faker:"-"`
	PendingUpdate     bool
	BlockchainAddress string    `faker:"blockchainAddress"`
	DateRegistered    time.Time `faker:"2022Date"`
	DaysEnrolled      int       `faker:"boundary_start=0, boundary_end=100"`
	ProductCategory   string    `faker:"oneof: education, environment, faith, fuel/energy, government, farming, food, labour, health, other, saving, shop"`
	Gender            string    `faker:"oneof: male, female"`
	// TODO Age and DOB go with dob and compute age buckets
	Age               int `faker:"boundary_start=18, boundary_end=100"`
	DateOfBirth       time.Time
	AreaName          string `faker:"oneof: kilifi, kinango-kwale, kisauni, kitui, nairobi, mombasa, nyanza"`
	AreaType          string `faker:"oneof: urban, periurban, rural"`
	AccountType       string `faker:"oneof: individual, disbursement"`
	PreferredLanguage string `faker:"oneof: english, swahili"`
	IsMarketEnabled   bool
	TokenBalances     []BalanceEntity `gorm:"foreignKey:UserEntityID;constraint:OnDelete:CASCADE" faker:"getBalances"`
	OvolIn            *bigint.Bigint  `fake:"bigInt"`
	OvolOut           *bigint.Bigint  `fake:"bigInt"`
	OtxnsIn           int
	OtxnsOut          int
	OuniqueIn         int
	OuniqueOut        int
	SvolIn            *bigint.Bigint `faker:"bigInt"`
	SvolOut           *bigint.Bigint `faker:"bigInt"`
	StxnsIn           int
	StxnsOut          int
	SuniqueIn         int
	SuniqueOut        int
	SuniqueAll        int
	LastTradeOut      time.Time
	FirstTradeInUser  string `faker:"blockchainAddress"`
	FirstTradeInTime  time.Time
	CreatedAt         time.Time `gorm:"column:created_at" faker:"-"`
	UpdatedAt         time.Time `gorm:"column:updated_at" faker:"-"`
}

// implements Tabler for GORM table names
func (UserEntity) TableName() string {
	return "users"
}

func (user *UserEntity) GetHeaders() []string {
	return []string{
		"ID",
		"UpdatedAt",
		"PendingUpdate",
		"BlockchainAddress",
		"DateRegistered",
		"DaysEnrolled",
		"ProductCategory",
		"Gender",
		"Age",
		"AreaName",
		"AreaType",
		"AccountType",
		"PreferredLanguage",
		"IsMarketEnabled",
		"TokenBalance",
		"OvolIn",
		"OvolOut",
		"OtxnsIn",
		"OtxnsOut",
		"OuniqueIn",
		"OuniqueOut",
		"SvolIn",
		"SvolOut",
		"StxnsIn",
		"StxnsOut",
		"SuniqueIn",
		"SuniqueOut",
		"SuniqueAll",
		"LastTradeOut",
		"FirstTradeInUser",
		"FirstTradeInTime",
	}
}

func (user *UserEntity) ToSlice() []string {
	return []string{
		strconv.Itoa(int(user.ID)),
		date.Format(user.UpdatedAt),
		strconv.FormatBool(user.PendingUpdate),
		user.BlockchainAddress,
		date.Format(user.DateRegistered),
		strconv.Itoa(user.DaysEnrolled),
		user.ProductCategory,
		user.Gender,
		strconv.Itoa(user.Age),
		user.AreaName,
		user.AreaType,
		user.AccountType,
		user.PreferredLanguage,
		strconv.FormatBool(user.IsMarketEnabled),
		user.OvolIn.String(),
		//user.TokenBalance.String(),
		user.OvolOut.String(),
		strconv.Itoa(user.OtxnsIn),
		strconv.Itoa(user.OtxnsOut),
		strconv.Itoa(user.OuniqueIn),
		strconv.Itoa(user.OuniqueOut),
		user.SvolIn.String(),
		user.SvolOut.String(),
		strconv.Itoa(user.StxnsIn),
		strconv.Itoa(user.StxnsOut),
		strconv.Itoa(user.SuniqueIn),
		strconv.Itoa(user.SuniqueOut),
		strconv.Itoa(user.SuniqueAll),
		date.Format(user.LastTradeOut),
		user.FirstTradeInUser,
		date.Format(user.FirstTradeInTime),
	}
}

// Creates a new user
func NewUser(address *common.Address) *UserEntity {
	user := UserEntity{
		BlockchainAddress: address.String(),
	}
	// setting up defaults for the bigint values
	defaultValue := bigint.NewBigint(big.NewInt(0))
	user.OvolIn = defaultValue
	user.OvolOut = defaultValue
	user.SvolIn = defaultValue
	user.SvolOut = defaultValue

	defaultUnknown := "unknown"
	// set default values
	user.Gender = defaultUnknown
	user.ProductCategory = defaultUnknown
	user.AreaName = defaultUnknown
	user.AreaType = defaultUnknown

	return &user
}

func MergeMetaResponsePerson(user *UserEntity, m *meta.MetaResponsePerson) *UserEntity {
	// set values from meta response if they are present
	user.DateOfBirth = time.Date(m.DateOfBirth.Month, time.Month(m.DateOfBirth.Month), m.DateOfBirth.Day, 0, 0, 0, 0, &time.Location{})
	user.Gender = util.GenderMapper(m.Gender)
	user.DateRegistered = time.Unix(int64(m.DateRegistered), 0)
	if len(m.Products) > 0 {
		user.ProductCategory = getKeyByValue(m.Products[0], "product_categories")
	}
	mappedAreaName := getKeyByValue(m.Location.AreaName, "area_names")
	user.AreaName = mappedAreaName
	user.AreaType = getKeyByValue(mappedAreaName, "area_types")
	return user
}

func MergeMetaResponseCustom(user *UserEntity, m *meta.MetaResponseCustom) *UserEntity {
	user.AccountType = m.AccountType
	return user
}

func MergeMetaResponsePreferences(user *UserEntity, m *meta.MetaResponsePreferences) *UserEntity {
	user.PreferredLanguage = m.PreferredLanguage
	user.IsMarketEnabled = m.IsMarketEnabled
	return user
}

type Users []*UserEntity

type UserCollections []*UserCollection

//MOVED see cic/meta/meta.go
// response from cic met cic.person type
type UserCollection struct {
	DateRegistered int         `json:"date_registered"`
	Gender         string      `json:"gender"`
	Identity       Identities  `json:"identities"`
	Location       Location    `json:"location"`
	Products       []string    `json:"products"`
	DateOfBirth    DateOfBirth `json:"date_of_birth"`
}

type Identities struct {
	Evm Evm `json:"evm"`
}

type Evm struct {
	Bloxberg []string `json:"bloxberg:8996"`
}

type Location struct {
	AreaName string `json:"area_name"`
}

type DateOfBirth struct {
	Year  int `json:"year"`
	Month int `json:"month"`
	Day   int `json:"day"`
}

type UserPreferences struct {
	PreferredLanguage string `json:"preferred_language"`
	IsMarketEnabled   bool   `json:"is_market_enabled"`
}

func (userCollection *UserCollection) ToUser() (*UserEntity, error) {
	if len(userCollection.Identity.Evm.Bloxberg) == 0 {
		return nil, errors.New("empty blockchain address")
	}

	// data calculation
	blockchainAddress := userCollection.Identity.Evm.Bloxberg[0]
	dateRegistered := date.ParseUnixTimestamp(int64(userCollection.DateRegistered))
	daysEnrolled := calculateDaysEnrolled(userCollection.DateRegistered)
	productCategory := getKeyByValue(userCollection.Products[0], "product_categories")
	gender := userCollection.Gender
	age := calculateAge(userCollection.DateOfBirth)
	areaName := getKeyByValue(userCollection.Location.AreaName, "area_names")
	areaType := getKeyByValue(areaName, "area_types")
	//updatedAt := time.Now()

	return &UserEntity{
		BlockchainAddress: blockchainAddress,
		DateRegistered:    dateRegistered,
		DaysEnrolled:      daysEnrolled,
		ProductCategory:   productCategory,
		Gender:            gender,
		Age:               age,
		AreaName:          areaName,
		AreaType:          areaType,
		PendingUpdate:     false,
		//UpdatedAt:         updatedAt,
	}, nil
}

func calculateDaysEnrolled(dateRegistered int) int {
	enrolledDay := date.ParseUnixTimestamp(int64(dateRegistered))
	diff := time.Now().Sub(enrolledDay)
	hrs := diff.Hours()
	days := int(math.Floor(hrs / 24))
	return days
}

func calculateAge(dateOfBirth DateOfBirth) int {
	year := dateOfBirth.Year
	month := dateOfBirth.Month
	day := dateOfBirth.Day

	if year == 0 {
		return 0
	}

	if month == 0 {
		month = 1
	}

	if day == 0 {
		day = 1
	}

	userBirthday := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.UTC)
	diff := time.Now().Sub(userBirthday)
	days := diff.Hours() / 24
	age := int(math.Floor(days / 365))

	return age
}

// TODO this looks like a helper but its an http request
// we will want a runtime configuration for the paths now in common/config/schema
func getKeyByValue(value string, category string) string {
	var jsonUrl string
	var jsonBytes []byte

	switch category {
	case "area_names":
		jsonUrl = config.Conf.UserResource.AreaNameHost
	case "area_types":
		jsonUrl = config.Conf.UserResource.AreaTypeHost
	case "product_categories":
		jsonUrl = config.Conf.UserResource.ProductCategoryHost
	}
	if jsonUrl == "" {
		return "unknown"
	}

	u, err := url.Parse(jsonUrl)
	if err != nil {
		log.Println(err.Error())
		return "unknown"
	}

	if u.Scheme == "file" {
		jsonFile, err := os.Open(u.Path)
		if err != nil {
			log.Println(err.Error())
			return "unknown"
		}

		defer jsonFile.Close()

		inputBytes, err := ioutil.ReadAll(jsonFile)
		jsonBytes = inputBytes
		if err != nil {
			log.Println(err.Error())
			return "unknown"
		}
	} else {
		// accept both http and https
		response, err := http.Get(jsonUrl)
		if err != nil {
			log.Println(err.Error())
			return "unknown"
		}

		responseData, err := ioutil.ReadAll(response.Body)
		if err != nil {
			log.Println(err.Error())
			return "unknown"
		}
		jsonBytes = responseData
	}

	var jsonObj map[string]interface{}
	json.Unmarshal(jsonBytes, &jsonObj)

	keyToValue := util.InterfaceToMapSlice(jsonObj[category])
	result := util.GetKeyByValue(keyToValue, value)

	return result
}
