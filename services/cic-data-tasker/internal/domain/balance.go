package domain

import (
	"time"

	"gitlab.com/grassrootseconomics/cic-data-golang/common/bigint"
)

type BalanceEntity struct {
	ID           int            `gorm:"column:id;primaryKey" faker:"-"`
	UserEntityID int            `gorm:"column:user_id;index:idx_member" faker:"-"`
	TokenAddress string         `gorm:"column:token_address;index:idx_member"`
	Amount       *bigint.Bigint `gorm:"column:amount" faker:"bigInt"`
	CreatedAt    time.Time      `gorm:"column:created_at" faker:"-"`
	UpdatedAt    time.Time      `gorm:"column:updated_at" faker:"-"`
}

// implements Tabler for GORM table names
func (BalanceEntity) TableName() string {
	return "balances"
}
