package domain

import (
	"log"
	"strconv"
	"time"

	"gitlab.com/grassrootseconomics/cic-data-golang/common/bigint"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/date"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/web3"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/config"
)

type TransactionEntity struct {
	ID                      int `gorm:"column:id;primaryKey"`
	TransactionHash         string
	BlockNumber             int
	BlockTime               time.Time `faker:"2022Date"`
	SenderAddress           string
	ReceiverAddress         string
	AmountSent              *bigint.Bigint `faker:"transactionAmount"`
	AmountReceived          *bigint.Bigint
	TransactionType         string `faker:"oneof: standard, disbursement, reclamation"`
	SenderToken             string `faker:"-"`
	ReceiverToken           string `faker:"-"`
	SenderRegistered        time.Time `faker:"2022Date"`
	SenderProductCategory   string `faker:"oneof: education, environment, faith, fuel/energy, government, farming, food, labour, health, other, saving, shop"`
	SenderGender            string `faker:"oneof: male, female"`
	SenderAge               int    `faker:"boundary_start=18, boundary_end=100"`
	SenderAreaName          string `faker:"oneof: kilifi, kinango-kwale, kisauni, kitui, nairobi, mombasa, nyanza"`
	SenderAreaType          string `faker:"oneof: urban, periurban, rural"`
	ReceiverRegistered      time.Time `faker:"2022Date"`
	ReceiverProductCategory string `faker:"oneof: education, environment, faith, fuel/energy, government, farming, food, labour, health, other, saving, shop"`
	ReceiverGender          string `faker:"oneof: male, female"`
	ReceiverAge             int    `faker:"boundary_start=18, boundary_end=100"`
	ReceiverAreaName        string `faker:"oneof: kilifi, kinango-kwale, kisauni, kitui, nairobi, mombasa, nyanza"`
	ReceiverAreaType        string `faker:"oneof: urban, periurban, rural"`
	CreatedAt               time.Time `gorm:"column:created_at"` 
	UpdatedAt               time.Time `gorm:"column:updated_at"`
}

// implements Tabler for GORM table names
func (TransactionEntity) TableName() string {
	return "transactions"
}

func (t *TransactionEntity) GetHeaders() []string {
	return []string{
		"ID",
		"TransactionHash",
		"BlockNumber",
		"BlockTime",
		"SenderAddress",
		"ReceiverAddress",
		"AmountSent",
		"AmountReceived",
		"TransactionType",
		"SenderToken",
		"ReceiverToken",
		"SenderRegistered",
		"SenderProductCategory",
		"SenderGender",
		"SenderAge",
		"SenderAreaName",
		"SenderAreaType",
		"ReceiverRegistered",
		"ReceiverProductCategory",
		"ReceiverGender",
		"ReceiverAge",
		"ReceiverAreaName",
		"ReceiverAreaType",
	}
}

func (t *TransactionEntity) ToSlice() []string {
	return []string{
		strconv.Itoa(t.ID),
		t.TransactionHash,
		strconv.Itoa(t.BlockNumber),
		date.Format(t.BlockTime),
		t.SenderAddress,
		t.ReceiverAddress,
		t.AmountSent.String(),
		t.AmountReceived.String(),
		t.TransactionType,
		t.SenderToken,
		t.ReceiverToken,
		date.Format(t.SenderRegistered),
		t.SenderProductCategory,
		t.SenderGender,
		strconv.Itoa(t.SenderAge),
		t.SenderAreaName,
		t.SenderAreaType,
		date.Format(t.ReceiverRegistered),
		t.ReceiverProductCategory,
		t.ReceiverGender,
		strconv.Itoa(t.ReceiverAge),
		t.ReceiverAreaName,
		t.ReceiverAreaType,
	}
}

func (transactionCollection *TransactionCollection) ToTransaction(sender *UserEntity, receiver *UserEntity, w *web3.Web3) TransactionEntity {
	// data calculation
	transactionHash := transactionCollection.TxHash
	blockNumber := transactionCollection.BlockNumber
	blockTime := date.ParseUnixTimestamp(int64(transactionCollection.DateBlock))
	senderAddress := sender.BlockchainAddress
	receiverAddress := receiver.BlockchainAddress
	amountSent := transactionCollection.FromValue
	amountReceived := transactionCollection.ToValue
	transactionType := getTransactionType(transactionCollection, w)
	senderToken := transactionCollection.SourceToken
	receiverToken := transactionCollection.DestinationToken
	senderRegistered := sender.DateRegistered
	senderProductCategory := sender.ProductCategory
	senderGender := sender.Gender
	senderAge := sender.Age
	senderAreaName := sender.AreaName
	senderAreaType := sender.AreaType
	receiverRegistered := receiver.DateRegistered
	receiverProductCategory := receiver.ProductCategory
	receiverGender := receiver.Gender
	receiverAge := receiver.Age
	receiverAreaName := receiver.AreaName
	receiverAreaType := receiver.AreaType

	return TransactionEntity{
		TransactionHash:         transactionHash,
		BlockNumber:             blockNumber,
		BlockTime:               blockTime,
		SenderAddress:           senderAddress,
		ReceiverAddress:         receiverAddress,
		AmountSent:              amountSent,
		AmountReceived:          amountReceived,
		TransactionType:         transactionType,
		SenderToken:             senderToken,
		ReceiverToken:           receiverToken,
		SenderRegistered:        senderRegistered,
		SenderProductCategory:   senderProductCategory,
		SenderGender:            senderGender,
		SenderAge:               senderAge,
		SenderAreaName:          senderAreaName,
		SenderAreaType:          senderAreaType,
		ReceiverRegistered:      receiverRegistered,
		ReceiverProductCategory: receiverProductCategory,
		ReceiverGender:          receiverGender,
		ReceiverAge:             receiverAge,
		ReceiverAreaName:        receiverAreaName,
		ReceiverAreaType:        receiverAreaType,
	}
}

type Transactions []*TransactionEntity

type TransactionGetData struct {
	TransactionCollections TransactionCollections `json:"data"`
}

type TransactionCollections []*TransactionCollection

type TransactionCollection struct {
	BlockNumber int            `json:"block_number"`
	TxHash      string         `json:"tx_hash"`
	DateBlock   float64        `json:"date_block"`
	Sender      string         `json:"sender"`
	Recipient   string         `json:"recipient"`
	FromValue   *bigint.Bigint `json:"from_value"`
	ToValue     *bigint.Bigint `json:"to_value"`

	SourceToken      string `json:"source_token"`
	DestinationToken string `json:"destination_token"`
	Success          bool   `json:"success"`
	TxType           string `json:"tx_type"`
}

func getTransactionType(transactionCollection *TransactionCollection, w *web3.Web3) string {
	trustAddress := config.Conf.SystemAddress.TrustAddress
	//faucetAddress := config.Conf.SystemAddress.FaucetAddress
	faucetCommonAddress, err := w.GetContractAddress("Faucet")
	if err != nil {
		log.Println("Faucet contract cannot be found")
	}
	faucetAddress := faucetCommonAddress.String()

	if transactionCollection.Sender == trustAddress || transactionCollection.Sender == faucetAddress {
		return "disbursement"
	} else if transactionCollection.Recipient == trustAddress || transactionCollection.Recipient == faucetAddress {
		return "reclamation"
	} else {
		return "standard"
	}
}
