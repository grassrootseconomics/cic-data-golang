package domain

import (
	"fmt"
	"path"
	"runtime"
	"testing"
	"time"

	"encoding/json"

	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/config"

	"gitlab.com/grassrootseconomics/cic-data-golang/common/cic/meta"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/util/testutils"
)

var (
	cic_mappings_dir string
)

// sets the dir name of the cic mappings files
func init() {
	_, filename, _, _ := runtime.Caller(0)
	cic_mappings_dir = path.Join(path.Dir(filename), "../../../../config/cic/mappings/")
}

//TODO we shouldn't rely on any external network calls for our unit tests, especially this dev server
func TestJsonHttpUrl(t *testing.T) {
	url := "https://ic3.dev/cic/mappings/area_names.json"
	config.Conf.UserResource.AreaNameHost = url

	value, category := "makongeni", "area_names"
	got := getKeyByValue(value, category)
	expect := "kilifi"

	if got != expect {
		t.Errorf("got %q, wanted %q", got, expect)
	}

	url = "https://ic3.dev/cic/mappings/area_types.json"
	config.Conf.UserResource.AreaTypeHost = url

	value, category = "kilifi", "area_types"
	got = getKeyByValue(value, category)
	expect = "periurban"

	if got != expect {
		t.Errorf("got %q, wanted %q", got, expect)
	}

	url = "https://ic3.dev/cic/mappings/product_categories.json"
	config.Conf.UserResource.ProductCategoryHost = url

	value, category = "academy", "product_categories"
	got = getKeyByValue(value, category)
	expect = "education"

	if got != expect {
		t.Errorf("got %q, wanted %q", got, expect)
	}
}

func TestJsonFileUrl(t *testing.T) {
	dirname := cic_mappings_dir
	url := fmt.Sprintf("file://%s/area_names.json", dirname)
	config.Conf.UserResource.AreaNameHost = url

	value, category := "makongeni", "area_names"
	got := getKeyByValue(value, category)
	expect := "kilifi"

	if got != expect {
		t.Errorf("got %q, wanted %q", got, expect)
	}

	url = fmt.Sprintf("file://%s/area_types.json", dirname)
	config.Conf.UserResource.AreaTypeHost = url

	value, category = "kilifi", "area_types"
	got = getKeyByValue(value, category)
	expect = "periurban"

	if got != expect {
		t.Errorf("got %q, wanted %q", got, expect)
	}

	url = fmt.Sprintf("file://%s/product_categories.json", dirname)
	config.Conf.UserResource.ProductCategoryHost = url

	value, category = "academy", "product_categories"
	got = getKeyByValue(value, category)
	expect = "education"

	if got != expect {
		t.Errorf("got %q, wanted %q", got, expect)
	}
}

func TestMergeMetaResponsePerson(t *testing.T) {
	dirname := cic_mappings_dir
	url := fmt.Sprintf("file://%s/area_types.json", dirname)
	config.Conf.UserResource.AreaTypeHost = url

	user := UserEntity{
		BlockchainAddress: testutils.GenerateEthAddress().String(),
	}

	metaResponse := meta.MetaResponsePerson{
		DateRegistered: int(time.Now().Unix()),
		Gender:         "female",
		Identity: meta.Identities{
			Evm: meta.Evm{
				Bloxberg: []string{"bloxberg:8996"},
			},
		},
		Location: meta.Location{
			AreaName: "makongeni",
		},
		Products: []string{"cyber"},
	}

	updatedUser := MergeMetaResponsePerson(&user, &metaResponse)

	t.Run("a known area name mapping", func(t *testing.T) {
		if updatedUser.AreaName != "kilifi" {
			t.Errorf("expected %s got %s", "kilifi", updatedUser.AreaName)
		}
	})

	t.Run("a known area type mapping", func(t *testing.T) {
		if updatedUser.AreaType != "periurban" {
			t.Errorf("expected area type to be 'perriurban' got %s", updatedUser.AreaType)
		}
	})
}

func TestCreateUser(t *testing.T) {
	testAddress := testutils.GenerateEthAddress()
	user := NewUser(testAddress)

	expected := UserEntity{
		BlockchainAddress: testAddress.String(),
	}

	if user.BlockchainAddress != expected.BlockchainAddress {
		t.Errorf("got: %v expected: %v", user, expected)
	}
}

func TestMergeMetaResponse(t *testing.T) {
	t.Run("merge a meta cic.person response into a user object without error", func(t *testing.T) {
		testAddress := testutils.GenerateEthAddress()
		user := NewUser(testAddress)

		metaResponse := meta.MetaResponsePerson{}
		metaResponseJson := "{\"date_registered\": 1619897179, \"gender\": \"male\", \"identities\": {\"evm\": {\"bloxberg\": {\"bloxberg:8996\": [\"12b56336ce14409c66da142c3ecfd98fe66c5d2c\"]}}}, \"location\": {\"area_name\": \"mnarani\", \"latitude\": -19.579414599126977, \"longitude\": 108.20844753098925}, \"products\": [\"teacher\"], \"vcard\": \"QkVHSU46VkNBUkQNClZFUlNJT046My4wDQpFTUFJTDpob2R6aWN0aW1vdGVqQGVtYWlsLnNpDQpGTjpIYW5zLUguXCwgUG90dGVyDQpOOlBvdHRlcjtIYW5zLUguOzs7DQpURUw7VFlQPUNFTEw6KzI1NDQ0Nzc0NTIwMw0KRU5EOlZDQVJEDQo=\"}"
		err := json.Unmarshal([]byte(metaResponseJson), &metaResponse)
		if err != nil {
			fmt.Printf("unable to marshal json \n%s", err)
		}
		mergedUser := MergeMetaResponsePerson(user, &metaResponse)
		if mergedUser.Gender != "male" {
			t.Errorf("expected Gender to be Male got: %s", mergedUser.Gender)
		}
	})

	t.Run("merge a meta cic.person response that is missing a field should not fail", func(t *testing.T) {
		testAddress := testutils.GenerateEthAddress()
		user := NewUser(testAddress)

		metaResponse := meta.MetaResponsePerson{}
		metaResponseJson := "{\"date_registered\": 1619897179, \"identities\": {\"evm\": {\"bloxberg\": {\"bloxberg:8996\": [\"12b56336ce14409c66da142c3ecfd98fe66c5d2c\"]}}}, \"location\": {\"area_name\": \"mnarani\", \"latitude\": -19.579414599126977, \"longitude\": 108.20844753098925}, \"products\": [\"teacher\"], \"vcard\": \"QkVHSU46VkNBUkQNClZFUlNJT046My4wDQpFTUFJTDpob2R6aWN0aW1vdGVqQGVtYWlsLnNpDQpGTjpIYW5zLUguXCwgUG90dGVyDQpOOlBvdHRlcjtIYW5zLUguOzs7DQpURUw7VFlQPUNFTEw6KzI1NDQ0Nzc0NTIwMw0KRU5EOlZDQVJEDQo=\"}"
		err := json.Unmarshal([]byte(metaResponseJson), &metaResponse)
		if err != nil {
			t.Error(err)
		}
		mergedUser := MergeMetaResponsePerson(user, &metaResponse)
		expected := "unknown"
		got := mergedUser.Gender
		if expected != got {
			t.Errorf("expected %s got %s", expected, got)
		}
	})

	t.Run("Unknown Gender should be converted to Gender: 'unkown'", func(t *testing.T) {
		testAddress := testutils.GenerateEthAddress()
		user := NewUser(testAddress)

		metaResponse := meta.MetaResponsePerson{}
		metaResponseJson := "{\"date_registered\": 1619897179, \"gender\": \"Unknown gender\", \"identities\": {\"evm\": {\"bloxberg\": {\"bloxberg:8996\": [\"12b56336ce14409c66da142c3ecfd98fe66c5d2c\"]}}}, \"location\": {\"area_name\": \"mnarani\", \"latitude\": -19.579414599126977, \"longitude\": 108.20844753098925}, \"products\": [\"teacher\"], \"vcard\": \"QkVHSU46VkNBUkQNClZFUlNJT046My4wDQpFTUFJTDpob2R6aWN0aW1vdGVqQGVtYWlsLnNpDQpGTjpIYW5zLUguXCwgUG90dGVyDQpOOlBvdHRlcjtIYW5zLUguOzs7DQpURUw7VFlQPUNFTEw6KzI1NDQ0Nzc0NTIwMw0KRU5EOlZDQVJEDQo=\"}"
		err := json.Unmarshal([]byte(metaResponseJson), &metaResponse)
		if err != nil {
			t.Error(err)
		}
		mergedUser := MergeMetaResponsePerson(user, &metaResponse)
		expected := "unknown"
		got := mergedUser.Gender
		if expected != got {
			t.Errorf("expected: %s got: %s", expected, got)
		}
	})
}
