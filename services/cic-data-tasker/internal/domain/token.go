package domain

import "time"

type TokenEntity struct {
	ID           int       `gorm:"column:id;primaryKey"`
	Name         string    `gorm:"column:name" faker:"oneof: Kibabo Currency, Community Coin, Chama coin, Local CIC, Kitui Coin, Kilifi Coin, Dwalla Dollars"`
	Symbol       string    `gorm:"column:symbol" faker:"len=3"`
	TokenAddress string    `gorm:"column:token_address;uniqueIndex"  faker:"blockchainAddress"`
	Location     string    `gorm:"column:location" faker:"oneof: kilifi, kinango-kwale, kisauni, kitui, nairobi, mombasa, nyanza"`
	Description  string    `gorm:"column:description" faker:"oneof: Local CIC, Co-op CIC, Syntropic Agroforestry, Merchant CIC, Community Supported Agriculture, CIC for solar energy, Transportation CIC"`
	Supply       int       `gorm:"column:supply" faker:"boundary_start=100, boundary_end=100000"`
	Expiration   time.Time `gorm:"column:expiration"`
	Endorsements int       `gorm:"column:endorsements" faker:"boundary_start=10, boundary_end=100"`
	CreatedAt    time.Time `gorm:"column:created_at" faker:"2022Date"`
	UpdatedAt    time.Time `gorm:"column:updated_at" faker:"2022Date"`
}

// implements Tabler for GORM table names
func (TokenEntity) TableName() string {
	return "tokens"
}
