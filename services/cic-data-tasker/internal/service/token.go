package service

import (
	"fmt"
	"math/big"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"go.uber.org/zap"

	"gitlab.com/grassrootseconomics/cic-data-golang/common/pg"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/web3"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/web3/accounts"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/internal/dao"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/internal/domain"
)

type TokenService struct {
	Web3    *web3.Web3
	Logger  *zap.Logger
	TokenPG *dao.TokenPG
}

func (t *TokenService) GetAllTokenAddresses() ([]*common.Address, error) {
	tokenRegistryAddr, err := t.Web3.GetContractAddress("TokenRegistry")
	if err != nil {
		return nil, fmt.Errorf("failed to fetch token registry in GetAllTokenAddresses: %s", err)
	}
	// TODO: accounts registry is the same interface as token registry, but having the wrongly named instance here will be confusing
	tokenRegistryInstance, err := accounts.NewAccounts(tokenRegistryAddr, t.Web3.GetClient())
	if err != nil {
		return nil, err
	}

	totalTokens, err := tokenRegistryInstance.EntryCount(&bind.CallOpts{})
	if err != nil {
		return nil, fmt.Errorf("cannot get total number of tokens: %v", err)
	}

	//fmt.Println("Total Tokens:", totalTokens)

	var allTokens []*common.Address
	one := big.NewInt(1)
	start := big.NewInt(0)
	for i := new(big.Int).Set(start); i.Cmp(totalTokens) < 0; i.Add(i, one) {
		token, err := tokenRegistryInstance.Entry(&bind.CallOpts{}, i)
		if err != nil {
			return nil, fmt.Errorf("cannot get entry: %v", err)
		}
		allTokens = append(allTokens, &token)
	}

	return allTokens, nil
}

func (t *TokenService) AddToken(token *common.Address) error {
	var err error
	db := pg.Client()
	tokenDeets, err := t.Web3.GetTokenDetails(token)
	if err != nil {
		return fmt.Errorf("TokenService -> AddToken cannot get token details %s", err)
	}
	newToken := domain.TokenEntity{
		Name:         tokenDeets.Name,
		Symbol:       tokenDeets.Symbol,
		TokenAddress: token.String(),
	}
	err = t.TokenPG.AddToken(db, &newToken)
	if err != nil {
		return fmt.Errorf("TokenService -> AddToken cannot add token to db %s", err)
	}
	return nil
}
