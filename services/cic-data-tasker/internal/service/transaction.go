package service

import (
	"log"

	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/config"

	"github.com/pkg/errors"
	"go.uber.org/zap"

	"gitlab.com/grassrootseconomics/cic-data-golang/common/pg"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/web3"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/internal/dao"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/internal/domain"
)

type TransactionService struct {
	UserPG        *dao.UserPG
	TransactionPG *dao.TransactionPG
	Web3          *web3.Web3
	Logger        *zap.Logger
}

func (s *TransactionService) ProcessAllTransaction(transactionCollections domain.TransactionCollections) error {
	db := pg.Client()

	faucetCommonAddress, err := s.Web3.GetContractAddress("Faucet")
	if err != nil {
		log.Println("faucet contract cannot be found")
	}
	faucetAddress := faucetCommonAddress.String()

	for _, transactionCollection := range transactionCollections {
		// zero value transactions from system addresses won't be imported
		if (transactionCollection.Sender == config.Conf.SystemAddress.TrustAddress || transactionCollection.Sender == faucetAddress) && transactionCollection.FromValue.ToInt64() == 0 && transactionCollection.ToValue.ToInt64() == 0 {
			continue
		}
		// check transaction existed or not
		searchTransactionData, err := s.TransactionPG.FindTransaction(db, transactionCollection.TxHash)
		if err != nil {
			return errors.Errorf("Service/Transaction -> ProcessAllTransaction Error -> %s", err)
		}
		if searchTransactionData.TransactionHash != "" {
			continue
		}

		sender, err := s.UserPG.FindUserByBlockchainAddress(db, transactionCollection.Sender, s.Web3)
		if err != nil {
			return errors.Errorf("Service/Transaction -> ProcessAllTransaction Error -> %s", err)
		}
		if sender.BlockchainAddress == "" {
			log.Printf("Service/Transaction -> ProcessAllTransaction -> Sender Not Found in DB -> blockchain_address= %s \n", transactionCollection.Sender)
			continue
		}
		receiver, err := s.UserPG.FindUserByBlockchainAddress(db, transactionCollection.Recipient, s.Web3)
		if err != nil {
			return errors.Errorf("Service/Transaction -> ProcessAllTransaction Error -> %s", err)
		}
		if receiver.BlockchainAddress == "" {
			log.Printf("Service/Transaction -> ProcessAllTransaction -> Receiver Not Found in DB -> blockchain_address= %s \n", transactionCollection.Recipient)
			continue
		}

		transaction := transactionCollection.ToTransaction(sender, receiver, s.Web3)

		isTransactionUnique, err := s.TransactionPG.IsTransactionUnique(db, sender.BlockchainAddress, receiver.BlockchainAddress)
		if err != nil {
			return errors.Errorf("Service/Transaction -> ProcessAllTransaction Error -> %s", err)
		}
		tx := db.Begin()
		err = s.TransactionPG.AddTransaction(tx, transaction)
		if err != nil {
			tx.Rollback()
			return errors.Errorf("Service/Transaction -> ProcessAllTransaction Error -> %s", err)
		}

		senderData, receiverData := attachTransactionToUser(sender, receiver, transaction, isTransactionUnique)
		if transaction.TransactionType == "standard" {
			err = s.UserPG.UpdateUser(tx, *senderData)
			if err != nil {
				tx.Rollback()
				return errors.Errorf("Service/Transaction -> ProcessAllTransaction Error -> %s", err)
			}
			err = s.UserPG.UpdateUser(tx, *receiverData)
			if err != nil {
				tx.Rollback()
				return errors.Errorf("Service/Transaction -> ProcessAllTransaction Error -> %s", err)
			}
		} else if transaction.TransactionType == "disbursement" {
			err = s.UserPG.UpdateUser(tx, *receiverData)
			if err != nil {
				tx.Rollback()
				return errors.Errorf("Service/Transaction -> ProcessAllTransaction Error -> %s", err)
			}
		} else if transaction.TransactionType == "reclamation" {
			err = s.UserPG.UpdateUser(tx, *senderData)
			if err != nil {
				tx.Rollback()
				return errors.Errorf("Service/Transaction -> ProcessAllTransaction Error -> %s", err)
			}
		}
		tx.Commit()
	}

	return nil
}

func (s *TransactionService) SelectHighestBlock() (int, error) {
	db := pg.Client()

	latestBlockNum, err := s.TransactionPG.SelectHighestBlock(db)
	if err != nil {
		return 0, errors.Errorf("Service/Transaction -> SelectHighestBlock Error -> %s", err)
	}

	return latestBlockNum, nil
}

func attachTransactionToUser(sender *domain.UserEntity, receiver *domain.UserEntity, transaction domain.TransactionEntity, isTransactionUnique bool) (*domain.UserEntity, *domain.UserEntity) {
	sender.LastTradeOut = transaction.BlockTime

	if transaction.TransactionType == "standard" {
		sender.SvolOut = sender.SvolOut.Add(transaction.AmountSent)
		sender.StxnsOut += 1

		// check if receiving User has had first receiver or not from another User
		if receiver.FirstTradeInUser == "" {
			receiver.FirstTradeInUser = sender.BlockchainAddress
			receiver.FirstTradeInTime = transaction.BlockTime
		}

		receiver.SvolIn = receiver.SvolIn.Add(transaction.AmountReceived)
		receiver.StxnsIn += 1

		if isTransactionUnique {

			sender.SuniqueOut += 1
			sender.SuniqueAll += 1
			receiver.SuniqueIn += 1
			receiver.SuniqueAll += 1
		}
	} else if transaction.TransactionType == "disbursement" {
		// only update receiver, because DISBURSEMENT means sender is System
		receiver.OvolIn = receiver.OvolIn.Add(transaction.AmountReceived)
		receiver.OtxnsIn += 1
		if isTransactionUnique {
			receiver.OuniqueIn += 1
		}
	} else if transaction.TransactionType == "reclamation" {
		// only update sender, because RECLAMATION means recipient is System
		sender.OvolOut = sender.OvolOut.Add(transaction.AmountSent)
		sender.OtxnsOut += 1
		if isTransactionUnique {
			sender.OuniqueOut += 1
		}
	}

	return sender, receiver
}
