package service

import (
	"encoding/json"
	"log"

	"gitlab.com/grassrootseconomics/cic-data-golang/common/web3"

	"github.com/ethereum/go-ethereum/common"
	"github.com/pkg/errors"
	"go.uber.org/zap"

	"gitlab.com/grassrootseconomics/cic-data-golang/common/bigint"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/pg"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/internal/dao"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/internal/domain"
)

type UserService struct {
	PG     *dao.UserPG
	Web3   *web3.Web3
	Logger *zap.Logger
}

// calls the Account Registry contract to get all of the users and then checks if they are in the database
// returns a user db object for each user not in the database
// TODO this could be optimized by using the entry id from the contract to keep a pointer to latest added user
func (u *UserService) GetNewUsersToAdd() ([]*domain.UserEntity, error) {
	db := pg.Client()
	addresses, err := u.Web3.GetAllAddrs()
	if err != nil {
		return nil, err
	}

	newAddresses := make([]*common.Address, 0, len(addresses))

	for _, address := range addresses {
		exists, err := u.PG.UserExists(db, address.String())
		if err != nil {
			return nil, err
		}
		if !*exists {
			newAddresses = append(newAddresses, address)
		}
	}
	newUsers := make([]*domain.UserEntity, len(newAddresses))
	for _, address := range newAddresses {
		newUsers = append(newUsers, &domain.UserEntity{BlockchainAddress: address.String()})
	}
	return newUsers, nil
}

func (s *UserService) ProcessAllUser(userCollections domain.UserCollections) error {
	db := pg.Client()
	for _, userCollection := range userCollections {
		user, err := userCollection.ToUser()
		if err != nil {
			log.Printf("cannot convert user %v", err)
			continue
		}

		// check user existed or not
		searchUserData, err := s.PG.FindUserByBlockchainAddress(db, user.BlockchainAddress, s.Web3)
		if err != nil {
			return errors.Errorf("Service/User -> ProcessAllUser Error -> %s", err)
		}
		if searchUserData.BlockchainAddress != "" {
			log.Println("I guess we just gonna skip this user?")
			//TODO we really just gonna skip this user? shouldn't we check for updates?
			//continue
		}

		// get user type
		var accountType string
		accountType, err = s.Web3.GetAccountType(web3.BlockchainAddress(user.BlockchainAddress))
		if err != nil {
			log.Printf("unable to retrieve account type from cic meta: %s", err)
		}
		user.AccountType = accountType

		// get user preferences
		var (
			preferredLanguage string
			isMarketEnabled   bool
		)
		preferredLanguage, isMarketEnabled, err = s.Web3.GetPreferences(web3.BlockchainAddress(user.BlockchainAddress))
		if err != nil {
			log.Printf("unable to retrieve account preferences from cic meta: %s", err)
		}
		user.PreferredLanguage = preferredLanguage
		user.IsMarketEnabled = isMarketEnabled

		// get user balances
		tokenPG := dao.TokenPG{}
		tokenEntities, err := tokenPG.GetAllTokens(db)
		if err != nil {
			log.Printf("User Service --> Process User --> could not get tokens %s", err)
		}

		// get token balance
		userCommonAddress := common.HexToAddress(user.BlockchainAddress)
		balanceEntities := []domain.BalanceEntity{}
		for _, tokenEntity := range tokenEntities {
			tokenCommonAddress := common.HexToAddress(tokenEntity.TokenAddress)
			balanceEntity := domain.BalanceEntity{}
			balanceAmount, err := s.Web3.GetUserTokenBalance(userCommonAddress, tokenCommonAddress)
			if err != nil {
				log.Printf("could not get token balance for token: %s and user: %s", tokenEntity.TokenAddress, user.BlockchainAddress)
			}
			balanceEntity.Amount = bigint.NewBigint(balanceAmount)
			balanceEntity.TokenAddress = tokenEntity.TokenAddress
		}

		user.TokenBalances = balanceEntities

		log.Printf("Prepared user %v", user)
		err = s.PG.AddUser(db, user)
		if err != nil {
			return errors.Errorf("Service/User -> ProcessAllUser Error -> %s", err)
		}
	}

	return nil
}

func (s *UserService) UpdatePendingUser() error {
	db := pg.Client()

	users, err := s.PG.FindPendingUser(db)
	if err != nil {
		return errors.Errorf("Service/User -> UpdatePendingUser Error -> %s", err)
	}

	for _, user := range users {
		var web3UserData []byte
		web3UserData, err = s.Web3.GetUser(web3.BlockchainAddress(user.BlockchainAddress))
		if err != nil {
			// return errors.Errorf("Service/User -> UpdatePendingUser Error -> %s", err)
			log.Printf("Service/User -> Web3 GetUser Error")
			continue
		}

		var userCollection domain.UserCollection

		err = json.Unmarshal(web3UserData, &userCollection)
		if err != nil {
			return errors.Errorf("Service/User -> UpdatePendingUser Error -> %s", err)
		}

		var userData *domain.UserEntity
		userData, err = userCollection.ToUser()
		if err != nil {
			s.Logger.Error("cannot convert user", zap.Error(err))
			continue
		}

		// choose the data we want to update
		user.DateRegistered = userData.DateRegistered
		user.DaysEnrolled = userData.DaysEnrolled
		user.ProductCategory = userData.ProductCategory
		user.Gender = userData.Gender
		user.Age = userData.Age
		user.AreaName = userData.AreaName
		user.AreaType = userData.AreaType
		user.PendingUpdate = false
		user.UpdatedAt = userData.UpdatedAt

		err = s.PG.UpdateUser(db, *user)
		if err != nil {
			return errors.Errorf("Service/User -> UpdatePendingUser Error -> %s", err)
		}
	}

	return nil
}

func (s *UserService) UpdateUserTokenBalance(tokenSvc *TokenService) error {
	db := pg.Client()

	users, err := s.PG.GetAllUser(db)
	if err != nil {
		return errors.Errorf("Service/User -> UpdateUserTokenBalance GetAllUser Error -> %s", err)
	}
	tokens, err := tokenSvc.GetAllTokenAddresses()
	if err != nil {
		return errors.Errorf("Service/User -> UpdateUserTokenBalance GetAllTokenAddresses Error -> %s", err)
	}

	for _, token := range tokens {
		for _, user := range users {
			tokenBalance, err := s.Web3.GetUserTokenBalance(common.HexToAddress(user.BlockchainAddress), *token)
			if err != nil {
				return errors.Errorf("Service/User -> UpdateUserTokenBalance Error -> %s", err)
			}

			err = s.PG.UpdateUserTokenBalance(db, user.BlockchainAddress, token.String(), bigint.NewBigint(tokenBalance))
			if err != nil {
				return errors.Errorf("Service/User -> UpdateUserTokenBalance Error -> %s", err)
			}
		}
	}

	return nil
}
