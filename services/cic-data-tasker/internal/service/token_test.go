package service

import (
	"context"
	"log"
	"testing"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/common/hexutil"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/ethereum/go-ethereum/rpc"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/httputil"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/util/mocks"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/web3"
)

func TestGetAllTokenAddresses(t *testing.T) {
	server := rpc.NewServer()
	server.RegisterName("eth", new(mocks.TestEth))
	defer server.Stop()
	client := rpc.DialInProc(server)
	testClient := ethclient.NewClient(client)
	defer testClient.Close()

	conf := web3.Conf{
		RpcProvider:     "http://mock-rpc:8888",
		MetaHost:        "mock-meta",
		MetaPort:        "9000",
		RegistryAddress: "0xDEADBEEF",
	}
	w, err := web3.NewWeb3(httputil.NewDefaultClient(), &conf)
	if err != nil {
		log.Print(err)
	}

	tokenSVC := TokenService{}
	tokenSVC.Web3 = w.WithEthClient(testClient)

	mockAddresses := []string{"0x68357742958b88c378847Bac9577Ed59e11D3f6F", "0x78357742958b88c378847Bac9577Ed59e11D3f6F"}
	mockCommonAddresses := make([]common.Address, len(mockAddresses))
	for i := range mockAddresses {
		mockCommonAddresses[i] = common.HexToAddress(mockAddresses[i])
	}

	mocks.GetCallFunc = func(ctx context.Context, args mocks.TransactionArgs, blockNrOrHash rpc.BlockNumberOrHash, overrides *mocks.StateOverride) (hexutil.Bytes, error) {
		// the account registry and token registry are the same interface
		ret, err := mocks.MockAccountRegistryContract(args.Data, mockCommonAddresses)
		if err != nil {
			log.Print(err)
		}
		return ret, nil
	}

	addresses, err := tokenSVC.GetAllTokenAddresses()
	if err != nil {
		t.Error(err)
	}
	if addresses[0].String() != mockAddresses[0] {
		t.Errorf("expected first adddress to be %s", mockAddresses[0])
	}
}
