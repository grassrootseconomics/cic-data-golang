package mocks

import (
	"log"
	"math/big"
	"math/rand"
	"reflect"
	"time"

	"github.com/bxcodec/faker/v3"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/bigint"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/util/testutils"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/internal/domain"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/internal/dao"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/pg"
)

var (
	tokenEntity domain.TokenEntity
)

func init() {
	tokenEntity = domain.TokenEntity{
		Name:         "Giftable",
		Symbol:       "GFT",
		TokenAddress: testutils.GenerateEthAddress().String(),
	}
}

type Balance struct {
	TokenAddress string         `faker:"blockchainAddress"`
	Amount       *bigint.Bigint `faker:"bigInt"`
}

func CustomGenerator() {
	err := faker.AddProvider("blockchainAddress", func(v reflect.Value) (interface{}, error) {
		return testutils.GenerateEthAddress().String(), nil
	})
	if err != nil {
		log.Printf("error setting up provider %s", err)
	}

	err = faker.AddProvider("bigInt", func(v reflect.Value) (interface{}, error) {
		rand.Seed(time.Now().UnixNano())
		i := rand.Intn(10000)
		return bigint.NewBigint(big.NewInt(int64(i))), nil
	})
	if err != nil {
		log.Printf("error setting up provider %s", err)
	}

	err = faker.AddProvider("getBalances", func(v reflect.Value) (interface{}, error) {
		rand.Seed(time.Now().UnixNano())
		i := rand.Intn(10000)

		var balances []domain.BalanceEntity

		db := pg.Client()
		tokenPG := dao.TokenPG{}
		tokenEntities, err := tokenPG.GetAllTokens(db)
		if err != nil {
			log.Printf("could not get tokens %s", err)
		}

		// get token balance
		for _, tokenEntity := range tokenEntities {
			//tokenCommonAddress := common.HexToAddress(tokenEntity.TokenAddress)
			balance := domain.BalanceEntity{
				TokenAddress: tokenEntity.TokenAddress,
				Amount:       bigint.NewBigint(big.NewInt(int64(i))),
			}
			balances = append(balances, balance)
		}
		
		return balances, nil
	})
	if err != nil {
		log.Printf("error setting up provider %s", err)
	}
	
	err = faker.AddProvider("2022Date", func(v reflect.Value) (interface{}, error) {
		min := time.Date(2022, 1, 0, 0, 0, 0, 0, time.UTC).Unix()
    	max := time.Now().Unix()
    	delta := max - min

    	sec := rand.Int63n(delta) + min
    	return time.Unix(sec, 0), nil
	})
	if err != nil {
		log.Printf("error setting up provider %s", err)
	}
	err = faker.AddProvider("transactionAmount", func(v reflect.Value) (interface{}, error) {
		i := rand.Intn(10000)
    	return bigint.NewBigint(big.NewInt(int64(i))), nil
	})
	if err != nil {
		log.Printf("error setting up provider %s", err)
	}
}
