// +build sql

package integration

import (
	cyrptorand "crypto/rand"
	"fmt"
	"log"
	"math/big"
	"math/rand"
	"os"
	"testing"

	"github.com/ethereum/go-ethereum/common"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/bigint"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/pg"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/server"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/web3"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/config"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/internal/dao"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/internal/domain"
	"gorm.io/gorm"
)

var (
	db *gorm.DB

	w3 *web3.Web3

	logger, err = server.NewZapLogger()

	fakeUser = domain.UserEntity{
		BlockchainAddress: generateEthAddress().String(),
		Gender:            "Male",
		Age:               29,
		AreaName:          "Seattle",
		AreaType:          "Urban",
		PreferredLanguage: "English",
		TokenBalances: []domain.BalanceEntity{
			{TokenAddress: giftToken.TokenAddress, Amount: bigint.NewBigint(big.NewInt(42))},
		},
	}

	giftToken = domain.TokenEntity{
		Name:         "Gifto",
		Symbol:       "GFT",
		TokenAddress: generateEthAddress().String(),
	}

	BigToken = domain.TokenEntity{
		Name:         "Big",
		Symbol:       "BIG",
		TokenAddress: generateEthAddress().String(),
	}
)

func TestMain(m *testing.M) {
	var err error
	// These values match pg bootstrap script /scripts/create_test_db.sql and are hard coded to prevent
	// modifing the production database. Change these with great care.
	config.Init()
	pg.Init(&pg.Config{
		HOST:     config.Conf.PG.HOST,
		USER:     config.Conf.PG.USER,
		PASSWORD: config.Conf.PG.PASSWORD,
		DBName:   config.Conf.PG.DBName,
		PORT:     config.Conf.PG.PORT,
	})
	db = pg.Client()

	// TODO commented for a minute but we'll need it to query contracts
	//w3, err = web3.NewWeb3(httputil.NewDefaultClient(), &web3.Conf{
	//	config.Conf.Web3Provider.RpcProvider,
	//	config.Conf.Web3Provider.RegistryAddress,
	//	config.Conf.DataProvider.UserHost,
	//	config.Conf.DataProvider.UserPort,
	//})
	//if err != nil {
	//	log.Panicf("✋ hol up, failed to initialize web3 client\n %s", err)
	//}

	db.Migrator().DropTable(&domain.UserEntity{}, &domain.TokenEntity{}, &domain.BalanceEntity{})
	err = db.AutoMigrate(&domain.UserEntity{},
		&domain.TokenEntity{},
		&domain.BalanceEntity{},
		&domain.TransactionEntity{})
	if err != nil {
		log.Panicf("✋ hol up, can't migrate schemas: %s", err)
	}

	os.Exit(m.Run())
}

// --- helper functions for testing ---

func generateEthAddress() *common.Address {
	b := make([]byte, 20)
	cyrptorand.Read(b)
	a := common.BytesToAddress(b)
	return &a
}

func generateAmount() bigint.Bigint {
	i := rand.Intn(10000)
	return *bigint.FromInt64(int64(i))
}

// --- db CRUD operations ---

func TestAddToken(t *testing.T) {
	tokenPG := dao.TokenPG{}
	err := tokenPG.AddToken(db, &giftToken)
	if err != nil {
		t.Errorf("Failed to add token: %s", err)
	}
}

func TestAddUser(t *testing.T) {
	userPG := dao.UserPG{}

	err := userPG.AddUser(db, &fakeUser)
	if err != nil {
		t.Errorf("Failed to add user: %s", err)
	}
}

func TestUpdateUserBalance(t *testing.T) {
	userPG := dao.UserPG{}

	err := userPG.UpdateUserTokenBalance(db, fakeUser.BlockchainAddress, giftToken.TokenAddress, bigint.NewBigint(big.NewInt(101)))
	if err != nil {
		t.Errorf("Failed to update user token balance %s", err)
	}
}

func TestFetchingUserBalance(t *testing.T) {
	var err error
	userPG := dao.UserPG{}

	balance, err := userPG.GetUserTokenBalance(db, fakeUser.BlockchainAddress, giftToken.TokenAddress)
	if err != nil {
		t.Errorf("Failed to get back the first test user balance %v", err)
	}
	fmt.Printf("fakeUser balance: %+v\n", balance)
	expected := big.NewInt(101)
	got := big.NewInt(balance.Amount.ToInt64())
	if got.Cmp(expected) != 0 {
		t.Errorf("User balance got: %v expected: %v", got, expected)
	}
}
