package controller

import (
	"bytes"
	"encoding/csv"
	"fmt"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-server/internal/domain"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-server/internal/service"
)

type UserController interface {
	GetAllUser(c *gin.Context)
	GetUser(c *gin.Context)
	UpdatePendingUser(c *gin.Context)
}

type userController struct {
	svc *service.UserService
}

func NewUser(svc *service.UserService) UserController {
	return &userController{
		svc: svc,
	}
}

func (controller *userController) GetAllUser(c *gin.Context) {
	users, err := controller.svc.GetAllUser()
	if err != nil {
		log.Println(err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{"message": "Internal Server Error"})
		return
	}

	csvData := make([][]string, 0)
	csvData = append(csvData, (&domain.User{}).GetHeaders())

	dataBytes := new(bytes.Buffer)
	wr := csv.NewWriter(dataBytes)

	for _, user := range users {
		csvData = append(csvData, user.ToSlice())
	}

	wr.WriteAll(csvData)
	wr.Flush()

	c.Writer.Header().Set("Content-Type", "text/csv")
	c.Writer.Header().Set("Content-Disposition", fmt.Sprintf("attachment;filename=%s", "users.csv"))
	c.String(http.StatusOK, dataBytes.String())
}

func (controller *userController) GetUser(c *gin.Context) {
	blockchainAddress := c.Param("blockchain_address")

	user, err := controller.svc.GetUser(blockchainAddress)
	if err != nil {
		log.Println(err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{"message": "Internal Server Error"})
		return
	}
	if user.BlockchainAddress == "" {
		c.JSON(404, gin.H{"code": "USER_NOT_FOUND", "message": "User not found"})
		return
	}

	csvData := make([][]string, 0)
	csvData = append(csvData, (&domain.User{}).GetHeaders())

	dataBytes := new(bytes.Buffer)
	wr := csv.NewWriter(dataBytes)

	csvData = append(csvData, user.ToSlice())

	wr.WriteAll(csvData)
	wr.Flush()

	c.Writer.Header().Set("Content-Type", "text/csv")
	c.Writer.Header().Set("Content-Disposition", fmt.Sprintf("attachment;filename=%s", "users.csv"))
	c.String(http.StatusOK, dataBytes.String())
}

func (controller *userController) UpdatePendingUser(c *gin.Context) {
	blockchainAddress := c.Param("blockchain_address")
	err := controller.svc.UpdatePendingUser(blockchainAddress)

	if err != nil {
		log.Println(err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{"message": "Internal Server Error"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "success", "status_code": 200})
}
