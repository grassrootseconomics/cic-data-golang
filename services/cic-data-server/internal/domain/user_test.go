package domain

import (
	"fmt"
	"testing"
	"path"
	"runtime"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-server/config"
)

var (
	cic_mappings_dir string
)

// sets the dir name of the cic mappings files
func init() {
	_, filename, _, _ := runtime.Caller(0)
	cic_mappings_dir = path.Join(path.Dir(filename), "../../../../config/cic/mappings/")
}

func TestJsonHttpUrl(t *testing.T) {
	url := "https://ic3.dev/cic/mappings/area_names.json"
	config.Conf.UserResource.AreaNameHost = url

	value, category := "makongeni", "area_names"
	got := getKeyByValue(value, category)
	expect := "kilifi"

	if got != expect {
		t.Errorf("got %q, wanted %q", got, expect)
	}
}

func TestJsonFileUrl(t *testing.T) {
	dirname := cic_mappings_dir
	// we don't have a data folder to store test data, so for now:
	// in your home dir
	// $ wget https://ic3.dev/cic/mappings/area_names.json
	url := fmt.Sprintf("file://%s/area_names.json", dirname)
	config.Conf.UserResource.AreaNameHost = url

	value, category := "makongeni", "area_names"
	got := getKeyByValue(value, category)
	expect := "kilifi"

	if got != expect {
		t.Errorf("got %q, wanted %q", got, expect)
	}
}
