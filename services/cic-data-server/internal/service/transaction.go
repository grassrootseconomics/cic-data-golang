package service

import (
	"github.com/pkg/errors"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-server/internal/dao"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-server/internal/domain"
	"go.uber.org/zap"
)

type TransactionService struct {
	PG     *dao.TransactionPG
	Logger *zap.Logger
}

func (s *TransactionService) GetTransactions(startTimestampInt int64, endTimestampInt int64) ([]*domain.Transaction, error) {
	transaction, err := s.PG.GetTransactions(startTimestampInt, endTimestampInt)
	if err != nil {
		return nil, errors.Errorf("Service/Transaction -> GetTransactions Error -> %s", err)
	}
	return transaction, nil
}

func (s *TransactionService) GetSpendersReport(startTimestampInt int64, endTimestampInt int64) ([]*domain.TransactionReport, error) {
	transactions, err := s.PG.GetSpendersReport(startTimestampInt, endTimestampInt)
	if err != nil {
		return nil, errors.Errorf("Service/Transaction -> GetSpendersReport Error -> %s", err)
	}
	return transactions, nil
}
