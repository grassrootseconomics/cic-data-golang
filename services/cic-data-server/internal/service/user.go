package service

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"log"
	"strings"

	"github.com/pkg/errors"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/pg"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/web3"
	"go.uber.org/zap"

	"github.com/emersion/go-vcard"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-server/config"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-server/internal/dao"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-server/internal/domain"
)

type UserService struct {
	PG     *dao.UserPG
	Web3   *web3.Web3
	Logger *zap.Logger
}

func (s *UserService) GetAllUser() ([]*domain.User, error) {
	users, err := s.PG.GetAllUser()
	if err != nil {
		return nil, errors.Errorf("Service/User -> GetAllUser Error -> %s", err)
	}
	for i, user := range users {
		// get Vcard
		var userData []byte
		userData, err = s.Web3.GetUser(web3.BlockchainAddress(user.BlockchainAddress))
		if err != nil {
			// return nil, errors.Errorf("Service/User -> GetAllUser Error -> %s", err)
			log.Printf("Service/User -> Web3 GetUser Error")
			continue
		}

		var userCollection domain.UserCollection

		err = json.Unmarshal(userData, &userCollection)
		if err != nil {
			return nil, errors.Errorf("Service/User -> GetAllUser Error -> %s", err)
		}

		// getUserPrivateData
		var sDec []byte
		sDec, err = base64.StdEncoding.DecodeString(userCollection.Vcard)
		if err != nil {
			return nil, errors.Errorf("Service/User -> GetAllUser Error -> %s", err)
		}
		f := bytes.NewReader(sDec)
		dec := vcard.NewDecoder(f)
		var card vcard.Card
		card, err = dec.Decode()
		if err != nil {
			return nil, errors.Errorf("Service/User -> GetAllUser Error -> %s", err)
		}

		vcardN := strings.Split(card.PreferredValue(vcard.FieldName), ";")
		if len(vcardN) < 2 {
			return nil, errors.Errorf("Service/User -> GetAllUser Error -> vcardN's values aren't correct")
		}

		users[i].FirstName = vcardN[0]
		users[i].LastName = vcardN[1]
		users[i].Phone = card.PreferredValue(vcard.FieldTelephone)
		users[i].Product = userCollection.Products[0]
		users[i].Location = userCollection.Location.AreaName
		blockchainUrl := user.BlockchainAddress[2:len(user.BlockchainAddress)]
		users[i].UserUrl = config.Conf.PrivateData.UserBaseUrl + blockchainUrl
	}
	return users, nil
}

func (s *UserService) GetUser(blockchainAddress string) (*domain.User, error) {
	user, err := s.PG.GetUser(blockchainAddress)
	if err != nil {
		return nil, errors.Errorf("Service/User -> GetUser Error -> %s -> ", err)
	}

	// get Vcard
	var userData []byte
	userData, err = s.Web3.GetUser(web3.BlockchainAddress(user.BlockchainAddress))
	if err != nil {
		return nil, errors.Errorf("Service/User -> GetUser Error -> %s", err)
	}

	var userCollection domain.UserCollection

	err = json.Unmarshal(userData, &userCollection)
	if err != nil {
		return nil, errors.Errorf("Service/User -> GetUser Error -> %s", err)
	}

	// getUserPrivateData
	var sDec []byte
	sDec, err = base64.StdEncoding.DecodeString(userCollection.Vcard)
	if err != nil {
		return nil, errors.Errorf("Service/User -> GetUser Error -> %s", err)
	}
	f := bytes.NewReader(sDec)
	dec := vcard.NewDecoder(f)
	var card vcard.Card
	card, err = dec.Decode()
	if err != nil {
		return nil, errors.Errorf("Service/User -> GetUser Error -> %s", err)
	}

	vcardN := strings.Split(card.PreferredValue(vcard.FieldName), ";")
	if len(vcardN) < 2 {
		return nil, errors.Errorf("Service/User -> GetUser Error -> vcardN's values aren't correct")
	}

	user.FirstName = vcardN[0]
	user.LastName = vcardN[1]
	user.Phone = card.PreferredValue(vcard.FieldTelephone)
	user.Product = userCollection.Products[0]
	user.Location = userCollection.Location.AreaName
	blockchainUrl := user.BlockchainAddress[2:len(user.BlockchainAddress)]
	user.UserUrl = config.Conf.PrivateData.UserBaseUrl + blockchainUrl

	return user, nil
}

func (s *UserService) UpdatePendingUser(blockchainAddress string) error {
	user, err := s.PG.GetUser(blockchainAddress)
	if user.BlockchainAddress == "" {
		err = s.PG.AddUser(blockchainAddress)
		if err != nil {
			return errors.Errorf("Service/User -> UpdatePendingUser Error -> %s", err)
		}
	}

	err = s.PG.UpdatePendingUser(blockchainAddress)
	if err != nil {
		return err
	}

	return nil
}

func (s *UserService) ProcessAllUser(userCollections domain.UserCollections) error {
	db := pg.Client()

	for _, userCollection := range userCollections {
		user, err := userCollection.ToUser()
		if err != nil {
			log.Printf("Error converting an user: %s \n", err)
			continue
		}

		// getUserType and Preferences
		var accountType string
		accountType, err = s.Web3.GetAccountType(web3.BlockchainAddress(user.BlockchainAddress))
		if err != nil {
			return errors.Errorf("Service/User -> ProcessAllUser Error -> %s", err)
		}
		user.AccountType = accountType

		var (
			preferredLanguage string
			isMarketEnabled   bool
		)
		preferredLanguage, isMarketEnabled, err = s.Web3.GetPreferences(web3.BlockchainAddress(user.BlockchainAddress))
		if err != nil {
			return errors.Errorf("Service/User -> ProcessAllUser Error -> %s", err)
		}
		user.PreferredLanguage = preferredLanguage
		user.IsMarketEnabled = isMarketEnabled

		// check user existed or not
		searchUserData, err := s.PG.FindUserByBlockchainAddress(db, user.BlockchainAddress)
		if err != nil {
			return errors.Errorf("Service/User -> ProcessAllUser Error -> %s", err)
		}
		if searchUserData.BlockchainAddress != "" {
			// check the following fields are same or not: product_category, age, area_name, area_type, preferred_language, is_market_enabled
			if searchUserData.ProductCategory != user.ProductCategory ||
				searchUserData.Age != user.Age ||
				searchUserData.AreaName != user.AreaName ||
				searchUserData.AreaType != user.AreaType ||
				searchUserData.PreferredLanguage != user.PreferredLanguage ||
				searchUserData.IsMarketEnabled != user.IsMarketEnabled {
				tx := db.Begin()
				err = s.PG.UpdateExistingUser(tx, user)
				if err != nil {
					tx.Rollback()
					return errors.Errorf("Service/User -> ProcessAllUser Error -> %s", err)
				}
				tx.Commit()
			}
			continue
		}

		tx := db.Begin()
		err = s.PG.AddFullUser(tx, user)
		if err != nil {
			tx.Rollback()
			return errors.Errorf("Service/User -> ProcessAllUser Error -> %s", err)
		}
		tx.Commit()
	}

	return nil
}
