package dao

import (
	"time"

	"github.com/pkg/errors"
	"gorm.io/gorm"

	"gitlab.com/grassrootseconomics/cic-data-golang/common/date"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/pg"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-server/config"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-server/internal/domain"
)

type UserPG struct{}

func (u *UserPG) GetAllUser() ([]*domain.User, error) {
	db := pg.Client()

	var users domain.Users
	if err := db.Raw("SELECT * FROM users ORDER BY id").Scan(&users).Error; err != nil {
		return nil, errors.Errorf("Repository/User -> GetAllUser Error -> %s", err)
	}

	return users, nil
}

func (u *UserPG) GetUser(blockchainAddress string) (*domain.User, error) {
	db := pg.Client()

	var user domain.User
	if err := db.Raw("SELECT * FROM users WHERE blockchain_address = ? ORDER BY id", blockchainAddress).Scan(&user).Error; err != nil {
		return nil, errors.Errorf("Repository/User -> GetUser Error -> %s", err)
	}

	return &user, nil
}

func (u *UserPG) UpdatePendingUser(blockchainAddress string) error {
	db := pg.Client()

	var user domain.User
	if err := db.Raw("UPDATE users SET pending_update = true WHERE blockchain_address = ?", blockchainAddress).Scan(&user).Error; err != nil {
		return errors.Errorf("Repository/User -> UpdatePendingUser Error -> %s", err)
	}

	return nil
}

func (u *UserPG) AddUser(blockchainAddress string) error {
	db := pg.Client()

	var user domain.User
	if err := db.Raw("INSERT INTO users(blockchain_address, updated_at) VALUES (?, ?)", blockchainAddress, date.Format(time.Now())).Scan(&user).Error; err != nil {
		return errors.Errorf("Repository/User -> AddUser Error -> %s", err)
	}

	return nil
}

func (u *UserPG) FindUserByBlockchainAddress(db *gorm.DB, blockchainAddress string) (*domain.User, error) {
	var user domain.User
	trustAddress := config.Conf.SystemAddress.TrustAddress
	faucetAddress := config.Conf.SystemAddress.FaucetAddress

	if blockchainAddress == trustAddress || blockchainAddress == faucetAddress {
		user = domain.User{
			BlockchainAddress: "SYSTEM_ADDRESS",
			DateRegistered:    time.Date(1970, time.Month(1), 1, 0, 0, 0, 0, time.UTC),
			Age:               0,
			ProductCategory:   "system",
			AreaType:          "system",
			AreaName:          "system",
			Gender:            "system",
		}
	} else {
		if err := db.Raw("SELECT * FROM users WHERE blockchain_address = ? ORDER BY id", blockchainAddress).Scan(&user).Error; err != nil {
			return nil, errors.Errorf("Repository/User -> FindUserByBlockchainAddress Error -> %s", err)
		}
	}

	return &user, nil
}

func (u *UserPG) AddFullUser(db *gorm.DB, user *domain.User) error {
	if err := db.Exec(
		"INSERT INTO users("+
			"blockchain_address,"+
			"date_registered,"+
			"days_enrolled,"+
			"product_category,"+
			"gender,"+
			"age,"+
			"area_name,"+
			"area_type,"+
			"account_type,"+
			"preferred_language,"+
			"is_market_enabled,"+
			"updated_at"+
			") VALUES ("+
			"@BlockchainAddress,"+
			"@DateRegistered,"+
			"@DaysEnrolled,"+
			"@ProductCategory,"+
			"@Gender,"+
			"@Age,"+
			"@AreaName,"+
			"@AreaType,"+
			"@AccountType,"+
			"@PreferredLanguage,"+
			"@IsMarketEnabled,"+
			"@UpdatedAt)",
		user).Error; err != nil {
		return errors.Errorf("Repository/User -> AddUser Error -> %s", err)
	}
	return nil
}

func (u *UserPG) UpdateExistingUser(db *gorm.DB, user *domain.User) error {
	if err := db.Exec(
		"UPDATE users SET "+
			"product_category = @ProductCategory, "+
			"age = @Age, "+
			"area_name = @AreaName, "+
			"area_type = @AreaType, "+
			"preferred_language = @PreferredLanguage, "+
			"is_market_enabled = @IsMarketEnabled, "+
			"updated_at = @UpdatedAt "+
			"WHERE blockchain_address = @BlockchainAddress",
		user).Error; err != nil {
		return errors.Errorf("Repository/User -> UpdateExistingUser Error -> %s", err)
	}
	return nil
}
