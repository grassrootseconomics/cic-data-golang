package pg

import (
	"fmt"
	"log"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var db *gorm.DB

type Config struct {
	HOST     string
	USER     string
	PASSWORD string
	DBName   string
	PORT     string
}

//TODO if you pass an empty string for password it garbles this dsn and fails to connect. plz fix.
func Init(config *Config) {
	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=UTC", config.HOST, config.USER, config.PASSWORD, config.DBName, config.PORT)
	log.Printf("postgres dsn: %s", dsn)
	var err error
	db, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Panic("error when trying to connect to PostgreSQL", err)
	}
	if db == nil {
		log.Panic("PostgresSQL failed to initialize")
	}
}

// TODO This panics if not initialized correctly. Please make this more resilient.
func Client() *gorm.DB {
	return db
}
