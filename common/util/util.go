package util

import (
	"crypto/sha256"
	"encoding/hex"
)

func InterfaceToMapSlice(inter interface{}) map[string][]string {
	obj := inter.(map[string]interface{})

	result := make(map[string][]string)

	for county, val := range obj {
		iList := val.([]interface{})
		areas := make([]string, len(iList))
		for _, val := range iList {
			areas = append(areas, val.(string))
		}
		result[county] = areas
	}

	return result
}

func GetKeyByValue(obj map[string][]string, value string) string {
	for key, list := range obj {
		for _, listItem := range list {
			if listItem == value {
				return key
			}
		}
	}

	return "unknown"
}

func MergeSha256Key(a []byte, b []byte) string {
	h := sha256.New()
	h.Write(a)
	h.Write(b)
	return hex.EncodeToString(h.Sum(nil))
}

func GenderMapper(gender string) string {
	switch gender {
	case "male":
		return gender
	case "female":
		return gender
	default:
		return "unknown"
	}
}
