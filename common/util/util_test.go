package util

import "testing"

func TestGetKeyByValue(t *testing.T) {
	t.Run("test simple area names", func(t *testing.T) {
		o := map[string][]string{
			"kilifi": []string{"ge", "makongeni"},
		}
		got := GetKeyByValue(o, "ge")
		expected := "kilifi"
		if got != expected {
			t.Errorf("expected %s got %s", expected, got)
		}
	})
}
