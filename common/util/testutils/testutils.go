package testutils

import (
	cyrptorand "crypto/rand"

	"github.com/ethereum/go-ethereum/common"
)

func GenerateEthAddress() *common.Address {
	b := make([]byte, 20)
	cyrptorand.Read(b)
	a := common.BytesToAddress(b)
	return &a
}
