package mocks

import (
	"context"
	"errors"
	"log"
	"math/big"
	"strconv"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/common/hexutil"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/ethereum/go-ethereum/rpc"
)

// --- shims for mocking web3 client from go-ethereum/nternal ---

type TransactionArgs struct {
	From                 *common.Address `json:"from"`
	To                   *common.Address `json:"to"`
	Gas                  *hexutil.Uint64 `json:"gas"`
	GasPrice             *hexutil.Big    `json:"gasPrice"`
	MaxFeePerGas         *hexutil.Big    `json:"maxFeePerGas"`
	MaxPriorityFeePerGas *hexutil.Big    `json:"maxPriorityFeePerGas"`
	Value                *hexutil.Big    `json:"value"`
	Nonce                *hexutil.Uint64 `json:"nonce"`

	// We accept "data" and "input" for backwards-compatibility reasons.
	// "input" is the newer name and should be preferred by clients.
	// Issue detail: https://github.com/ethereum/go-ethereum/issues/15628
	Data  *hexutil.Bytes `json:"data"`
	Input *hexutil.Bytes `json:"input"`

	// Introduced by AccessListTxType transaction.
	AccessList *types.AccessList `json:"accessList,omitempty"`
	ChainID    *hexutil.Big      `json:"chainId,omitempty"`
}

type OverrideAccount struct {
	Nonce     *hexutil.Uint64              `json:"nonce"`
	Code      *hexutil.Bytes               `json:"code"`
	Balance   **hexutil.Big                `json:"balance"`
	State     *map[common.Hash]common.Hash `json:"state"`
	StateDiff *map[common.Hash]common.Hash `json:"stateDiff"`
}

type StateOverride map[common.Address]OverrideAccount

// --- mocks for web3 objects --

type CallFunc func(ctx context.Context, args TransactionArgs, blockNrOrHash rpc.BlockNumberOrHash, overrides *StateOverride) (hexutil.Bytes, error)

type TestEth struct {
	callFunc CallFunc
	server   *rpc.Server
	client   *ethclient.Client
}

var (
	// TODO move all the tests to using the NewTestEth factory and stop exporting this
	GetCallFunc CallFunc
)

// From internal/ethapi
func (t *TestEth) Call(ctx context.Context, args TransactionArgs, blockNrOrHash rpc.BlockNumberOrHash, overrides *StateOverride) (hexutil.Bytes, error) {
	return GetCallFunc(ctx, args, blockNrOrHash, overrides)
}

// gets the client
func (t *TestEth) GetClient() *ethclient.Client {
	return t.client
}

// close rpc server and ethclient
func (t *TestEth) Close() {
	t.server.Stop()
	t.client.Close()
}

// create a new mock for ethclient.Client by providing a function matching the ethclient.Call signature
func NewTestEth(f CallFunc) *TestEth {
	server := rpc.NewServer()
	server.RegisterName("eth", new(TestEth))
	//defer server.Stop()
	client := rpc.DialInProc(server)
	testClient := ethclient.NewClient(client)
	//defer testClient.Close()

	GetCallFunc = f

	testEth := TestEth{
		server: server,
		client: testClient,
	}

	return &testEth
}

// Encode a string value into an ethclient reponse data. Max size 32 bytes.
func EncodeMockCallResponseData(data string) (hexutil.Bytes, error) {
	bytes := []byte(data)
	lenBytes := len(bytes)

	if lenBytes > 32 {
		return nil, errors.New("this function only supports serializing bytes lengths of less than 32")
	}

	lenBytesArray := make([]byte, 0, 32)
	arr := append(lenBytesArray, byte(lenBytes))
	padData := common.RightPadBytes(bytes, 32)
	padLenData := common.LeftPadBytes(arr, 32)

	// value start in the data array
	dataStartBytes := make([]byte, 0, 32)
	// assumption from the functions name, symbol and balance all starting at the first byte
	dataStartBytes = append(dataStartBytes, byte(32))
	padByteIndex := common.LeftPadBytes(dataStartBytes, 32)

	callReturn := append(padByteIndex, padLenData...)
	callReturn = append(callReturn, padData...)
	encoded := hexutil.Bytes(callReturn)
	return encoded, nil
}

func EncodeMockBigIntResponseData(data big.Int) (hexutil.Bytes, error) {
	dataBytes := data.Bytes()
	lenDataBytes := len(dataBytes)
	if lenDataBytes > 32 {
		return nil, errors.New("this function only supports serializing bytes lengths of less than 32")
	}
	padData := common.LeftPadBytes(dataBytes, 32)
	return padData, nil
}

func EncodeMockAddressResponseData(address *common.Address) (hexutil.Bytes, error) {
	b := make([]byte, 12)
	b = append(b, address.Bytes()...)
	return b, nil
}

func MockErc20Contract(argsData *hexutil.Bytes, name string, symbol string) (hexutil.Bytes, error) {
	nameHash := crypto.Keccak256Hash([]byte("name()")).Bytes()
	symbolHash := crypto.Keccak256Hash([]byte("symbol()")).Bytes()

	argsDataString := argsData.String()[:10]
	nameString := hexutil.Encode(nameHash)[:10]
	symbolString := hexutil.Encode(symbolHash)[:10]

	switch argsDataString {
	case nameString:
		ret, err := EncodeMockCallResponseData(name)
		if err != nil {
			return nil, err
		}
		return ret, nil
	case symbolString:
		ret, err := EncodeMockCallResponseData(symbol)
		if err != nil {
			return nil, err
		}
		return ret, nil
	default:
		return nil, errors.New("sorry I can't even fake like I know this method")
	}
}

// Detects the contract methods entry and entryCount and sends a mock response.
// mockAddresses: an array of addresses to return for entry()
func MockAccountRegistryContract(argsData *hexutil.Bytes, mockAddresses []common.Address) (hexutil.Bytes, error) {
	entryCountHash := crypto.Keccak256Hash([]byte("entryCount()")).Bytes()
	entryHash := crypto.Keccak256Hash([]byte("entry(uint256)")).Bytes()

	argsDataString := argsData.String()[:10]
	entryCountString := hexutil.Encode(entryCountHash)[:10]
	entryHashString := hexutil.Encode(entryHash)[:10]

	lenMockAddress := len(mockAddresses)
	switch argsDataString {
	case entryCountString:
		ret, err := EncodeMockBigIntResponseData(*big.NewInt(int64(lenMockAddress)))
		if err != nil {
			log.Print(err)
		}
		return ret, nil
	case entryHashString:
		entryId, err := strconv.ParseInt(argsData.String()[32:], 10, 0)
		if err != nil {
			log.Print(err)
		}
		ret, err := EncodeMockAddressResponseData(&mockAddresses[entryId])
		if err != nil {
			log.Print(err)
		}
		return ret, nil
	default:
		ret, err := EncodeMockCallResponseData("")
		if err != nil {
			log.Print(err)
		}
		return ret, nil
	}
}
