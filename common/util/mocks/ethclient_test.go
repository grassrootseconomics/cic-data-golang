package mocks

import (
	"context"
	"log"
	"math/big"
	"testing"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/common/hexutil"
	"github.com/ethereum/go-ethereum/rpc"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/web3/token"
)

func TestCreateMockCallResponse(t *testing.T) {
	result, err := EncodeMockCallResponseData("BZZ")
	if err != nil {
		t.Error(err)
	}
	expected := "0x00000000000000000000000000000000000000000000000000000000000000200000000000000000000000000000000000000000000000000000000000000003425a5a0000000000000000000000000000000000000000000000000000000000"
	if hexutil.Encode(result) != expected {
		t.Errorf("got: %s expected: %s", result, expected)
	}
}

func TestEncodeMockBigInt(t *testing.T) {

	expectedValue := big.NewInt(1000)
	f := func(ctx context.Context, args TransactionArgs, blockNrOrHash rpc.BlockNumberOrHash, overrides *StateOverride) (hexutil.Bytes, error) {
		ret, err := EncodeMockBigIntResponseData(*expectedValue)
		if err != nil {
			log.Print(err)
		}
		return ret, nil
	}

	testEth := NewTestEth(f)
	defer testEth.Close()
	testClient := testEth.GetClient()

	mockAddress := "0xDEADBEEF"

	instance, err := token.NewToken(common.HexToAddress(mockAddress), testClient)
	if err != nil {
		t.Error(err)
	}
	bal, err := instance.BalanceOf(&bind.CallOpts{}, common.HexToAddress("0xDEADBEEF1"))
	if err != nil {
		t.Error(err)
	}

	if expectedValue.Cmp(bal) != 0 {
		t.Errorf("expected %d got %d", expectedValue, bal)
	}
}
