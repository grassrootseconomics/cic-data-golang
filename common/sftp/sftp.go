package sftp

import (
	"github.com/pkg/sftp"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-server/config"
	"golang.org/x/crypto/ssh"
	"io"
	"os"
)

func Upload(filename string) error {

	// set config
	addr := config.Conf.SFTP.HOST + ":" + config.Conf.SFTP.PORT

	conf := &ssh.ClientConfig{
		User: config.Conf.SFTP.USER,
		Auth: []ssh.AuthMethod{
			ssh.Password(config.Conf.SFTP.PASSWORD),
		},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}

	// connect
	conn, err := ssh.Dial("tcp", addr, conf)
	if err != nil {
		return err
	}
	defer conn.Close()

	// create new SFTP client
	client, err := sftp.NewClient(conn)
	if err != nil {
		return err
	}
	defer client.Close()

	// create destination file
	dstFile, err := client.Create("./uploads/" + filename)
	if err != nil {
		return err
	}
	defer dstFile.Close()

	// create source file
	srcFile, err := os.Open("downloads/" + filename)
	if err != nil {
		return err
	}

	// copy source file to destination file
	_, err = io.Copy(dstFile, srcFile)
	if err != nil {
		return err
	}

	return nil
}
