package encrypt

import (
	"bytes"
	"compress/gzip"
	"io"
	"os"
	"strings"

	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-server/config"
	"golang.org/x/crypto/openpgp"
	"golang.org/x/crypto/openpgp/armor"
)

func Encrypt(messageReader *bytes.Buffer, fileName string) error {
	// Read in public key
	entityList, err := openpgp.ReadArmoredKeyRing(strings.NewReader(config.Conf.PGP.PublicKey))
	if err != nil {
		return err
	}

	// Create buffer to write output to
	buf := new(bytes.Buffer)

	// Create encoder
	encoderWriter, err := armor.Encode(buf, "PGP Message", make(map[string]string))
	if err != nil {
		return err
	}
	defer encoderWriter.Close()

	// Create encryptor with encoder
	encryptorWriter, err := openpgp.Encrypt(encoderWriter, entityList, nil, nil, nil)
	if err != nil {
		return err
	}
	defer encryptorWriter.Close()

	// Create compressor with encryptor
	compressorWriter, err := gzip.NewWriterLevel(encryptorWriter, gzip.BestCompression)
	if err != nil {
		return err
	}
	defer compressorWriter.Close()

	// Write message to compressor
	_, err = io.Copy(compressorWriter, messageReader)
	if err != nil {
		return err
	}

	// Write to a file
	f, err := os.Create("downloads/" + fileName)
	if err != nil {
		return err
	}
	defer f.Close()
	_, err = f.Write(buf.Bytes())
	if err != nil {
		return err
	}

	return nil
}
