package httputil

import (
	"net/http"
	"time"
)

type Config struct {
	Timeout time.Duration
}

var (
	defaultConfig = &Config{
		Timeout: time.Second * 10,
	}
)

type Client struct {
	client http.Client
}

func NewDefaultClient() *Client {
	return &Client{
		client: http.Client{
			Timeout: defaultConfig.Timeout,
		},
	}
}

func NewClient(config *Config) *Client {
	return &Client{
		client: http.Client{
			Timeout: config.Timeout,
		},
	}
}

func (c *Client) Get(url string) (*http.Response, error) {
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, err
	}

	res, err := c.client.Do(req)
	if err != nil {
		return nil, err
	}

	return res, nil
}
