package web3

import (
	"context"
	"log"
	"math/big"
	"testing"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/common/hexutil"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/ethereum/go-ethereum/rpc"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/httputil"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/util/mocks"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/web3/token"
)

var (
	conf = Conf{
		RpcProvider:     "http://mock-rpc:8545",
		RegistryAddress: "0xdeadbeef",
	}
)

func TestTokenSymbol(t *testing.T) {
	server := rpc.NewServer()
	server.RegisterName("eth", new(mocks.TestEth))
	defer server.Stop()
	client := rpc.DialInProc(server)
	testClient := ethclient.NewClient(client)
	defer testClient.Close()

	testCommonAddress := common.HexToAddress("0xdeadbeef")

	mocks.GetCallFunc = func(ctx context.Context, args mocks.TransactionArgs, blockNrOrHash rpc.BlockNumberOrHash, overrides *mocks.StateOverride) (hexutil.Bytes, error) {
		ret, err := mocks.EncodeMockCallResponseData("BZZ")
		if err != nil {
			t.Error(err)
		}
		return ret, nil
	}

	instance, err := token.NewToken(testCommonAddress, testClient)
	if err != nil {
		t.Error(err)
	}

	name, err := instance.Name(&bind.CallOpts{})
	if err != nil {
		t.Errorf("cannot get token contract Symbol: %v", err)
	}
	if name != "BZZ" {
		t.Errorf("expected BZZ got: %s", name)
	}
}

func TestGetBalance(t *testing.T) {
	server := rpc.NewServer()
	server.RegisterName("eth", new(mocks.TestEth))
	defer server.Stop()
	client := rpc.DialInProc(server)
	testClient := ethclient.NewClient(client)
	defer testClient.Close()

	testCommonAddress := common.HexToAddress("0xdeadbeef")

	expected := big.NewInt(400221)
	mocks.GetCallFunc = func(ctx context.Context, args mocks.TransactionArgs, blockNrOrHash rpc.BlockNumberOrHash, overrides *mocks.StateOverride) (hexutil.Bytes, error) {
		ret, err := mocks.EncodeMockBigIntResponseData(*expected)
		if err != nil {
			t.Error(err)
		}
		return ret, nil
	}

	instance, err := token.NewToken(testCommonAddress, testClient)
	if err != nil {
		t.Error(err)
	}

	balance, err := instance.BalanceOf(&bind.CallOpts{}, testCommonAddress)
	if err != nil {
		t.Error(err)
	}
	if expected.Cmp(balance) != 0 {
		t.Errorf("expected %d got: %d", expected, balance)
	}
}

func TestGetAllAddr(t *testing.T) {
	server := rpc.NewServer()
	server.RegisterName("eth", new(mocks.TestEth))
	defer server.Stop()
	client := rpc.DialInProc(server)
	testClient := ethclient.NewClient(client)
	defer testClient.Close()

	conf := Conf{
		RpcProvider:     "http://mock-rpc:8888",
		MetaHost:        "mock-meta",
		MetaPort:        "9000",
		RegistryAddress: "0xDEADBEEF",
	}
	w, err := NewWeb3(httputil.NewDefaultClient(), &conf)
	if err != nil {
		log.Print(err)
	}
	w.client = testClient

	mockAddresses := []string{"0x68357742958b88c378847Bac9577Ed59e11D3f6F", "0x78357742958b88c378847Bac9577Ed59e11D3f6F"}
	mockCommonAddresses := make([]common.Address, len(mockAddresses))
	for i := range mockAddresses {
		mockCommonAddresses[i] = common.HexToAddress(mockAddresses[i])
	}

	mocks.GetCallFunc = func(ctx context.Context, args mocks.TransactionArgs, blockNrOrHash rpc.BlockNumberOrHash, overrides *mocks.StateOverride) (hexutil.Bytes, error) {
		ret, err := mocks.MockAccountRegistryContract(args.Data, mockCommonAddresses)
		if err != nil {
			log.Print(err)
		}
		return ret, nil
	}

	addys, err := w.GetAllAddrs()
	if err != nil {
		t.Error(err)
	}
	expected := mockCommonAddresses[0].String()
	got := addys[0].String()
	if expected != got {
		t.Errorf("expected %s got %s", expected, got)
	}
	expected = mockCommonAddresses[1].String()
	got = addys[1].String()
	if expected != got {
		t.Errorf("expected %s got %s", expected, got)
	}
}
