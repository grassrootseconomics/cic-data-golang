package web3

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/big"
	"net/http"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/pkg/errors"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/cic/meta"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/web3/accounts"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/web3/registry"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/web3/token"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/config"
)

type ErrorResponse struct {
	StatusCode int     `json:"status_code"`
	Message    string  `json:"message"`
	Errors     []Error `json:"errors"`
}

// Error describes the error
type Error struct {
	Resource string `json:"resource"`
	Code     string `json:"code"`
	Field    string `json:"field"`
	Message  string `json:"message"`
}

// TODO this seems like string with extra steps? Try to use common.Address throughout
type BlockchainAddress string

type TokenDetails struct {
	Name   string
	Symbol string
}

func (b BlockchainAddress) String() string {
	return string(b)
}

type Conf struct {
	RpcProvider     string
	MetaHost        string
	MetaPort        string
	RegistryAddress string
}

type UserAccountType struct {
	AccountType string `json:"account_type"`
}

type UserPreferences struct {
	PreferredLanguage string `json:"preferred_language"`
	IsMarketEnabled   bool   `json:"is_market_enabled"`
}

type Getter interface {
	Get(url string) (*http.Response, error)
}

type Web3 struct {
	getter    Getter
	conf      *Conf
	contracts map[string]common.Address
	inited    bool
	client    *ethclient.Client
}

func (w *Web3) WithEthClient(ethclient *ethclient.Client) *Web3 {
	w.client = ethclient
	return w
}

// bring your own contract addresses, skips the contract lookups
func (w *Web3) WithContracts(contracts map[string]common.Address) *Web3 {
	w.inited = true
	w.contracts = contracts
	return w
}

// TODO remove the cic-meta details see cic/meta/meta.go
func NewWeb3(getter Getter, conf *Conf) (*Web3, error) {
	web3 := &Web3{
		getter:    getter,
		conf:      conf,
		contracts: make(map[string]common.Address),
		inited:    false,
	}

	registryCommonAddress := common.HexToAddress(conf.RegistryAddress)
	log.Printf("📝 Setting the contract registry addresses to: %v", registryCommonAddress)
	contractRegistryAddress := registryCommonAddress
	web3.contracts["ContractRegistry"] = contractRegistryAddress

	if conf.RpcProvider == "" {
		return web3, nil
	}

	client, err := ethclient.Dial(conf.RpcProvider)

	if err != nil {
		return nil, fmt.Errorf("cannot connect web3: %v", err)
	}
	log.Println("✅ connected to web3 client!")
	web3.client = client
	return web3, nil
}

func (w *Web3) GetClient() *ethclient.Client {
	return w.client
}

// TODO: Retrieve the identifier array from the contract directly
func (w *Web3) getContracts() error {
	registryAddress := common.HexToAddress(w.conf.RegistryAddress)
	registryInstance, err := registry.NewRegistry(registryAddress, w.client)
	if err != nil {
		log.Fatalf("Could not find the registry address: %s", registryAddress.String())
	}

	var registryInput [32]byte
	contractIdentifier := "AccountRegistry"
	registryIdentifierBytes := []byte(contractIdentifier)
	copy(registryInput[:], registryIdentifierBytes[0:15])
	addr, err := registryInstance.AddressOf(&bind.CallOpts{}, registryInput)
	if err != nil {
		log.Fatalf("error could not find AccountRegistry from ContractRegistry %s", err)
	}
	w.contracts[contractIdentifier] = addr

	contractIdentifier = "TokenRegistry"
	registryIdentifierBytes = []byte(contractIdentifier)
	copy(registryInput[:], registryIdentifierBytes[0:15])
	addr, err = registryInstance.AddressOf(&bind.CallOpts{}, registryInput)
	if err != nil {
		log.Fatalf("error could not find TokenRegistry from ContractRegistry %s", err)
	}
	w.contracts[contractIdentifier] = addr
	//logg.Println("Contract address", contractIdentifier, addr)

	contractIdentifier = "Faucet"
	registryIdentifierBytes = []byte(contractIdentifier)
	copy(registryInput[:], registryIdentifierBytes[0:15])
	addr, err = registryInstance.AddressOf(&bind.CallOpts{}, registryInput)
	if err != nil {
		log.Fatalf("error could not find Faucet from ContractRegistry %s", err)
	}
	w.contracts[contractIdentifier] = addr
	//logg.Println("Contract address", contractIdentifier, addr)
	w.inited = true

	log.Printf("fetched contracts %+v", w.contracts)

	return nil
}

func (w *Web3) GetContractAddress(s string) (common.Address, error) {
	if !w.inited {
		err := w.getContracts()
		if err != nil {
			return common.Address{}, err
		}
	}
	c, ok := w.contracts[s]
	if !ok {
		return common.Address{}, fmt.Errorf("no such contract: %s", s)
	}
	return c, nil
}

func (w *Web3) GetAllAddrs() ([]*common.Address, error) {
	// instantiate the accounts index
	addr, err := w.GetContractAddress("AccountRegistry")
	if err != nil {
		return nil, err
	}
	acctsIndexInstance, err := accounts.NewAccounts(addr, w.client)
	if err != nil {
		return nil, fmt.Errorf("cannot instantiate the accounts index: %v", err)
	}

	// get total number of accounts
	totalAccounts, err := acctsIndexInstance.EntryCount(&bind.CallOpts{})
	if err != nil {
		return nil, fmt.Errorf("cannot get total number of accounts: %v", err)
	}

	allAccounts := make([]*common.Address, 0)
	one := big.NewInt(1)
	start := big.NewInt(0)
	for accts := new(big.Int).Set(start); accts.Cmp(totalAccounts) < 0; accts.Add(accts, one) {
		acct, err := acctsIndexInstance.Entry(&bind.CallOpts{}, accts)
		if err != nil {
			return nil, fmt.Errorf("cannot get entry: %v", err)
		}
		allAccounts = append(allAccounts, &acct)
	}

	return allAccounts, nil
}

//TODO move to meta package and say get GetMetaPerson or something
// Get cic.person type from meta service
func (w *Web3) GetUser(s BlockchainAddress) ([]byte, error) {
	key, err := toAddressKey(s.String(), ":cic.person")
	if err != nil {
		return nil, errors.Errorf("web3 getUser toAddressKey error -> %s", err)
	}

	if config.Conf.MetaServiceUrl == "" {
		log.Fatal("MetaServiceUrl is not configured")
	}
	userUrl := fmt.Sprintf("%s/%s", config.Conf.MetaServiceUrl, key)

	res, err := w.getter.Get(userUrl)
	if err != nil {
		return nil, errors.Errorf("web3 GetUser error -> %s", err)
	}
	defer res.Body.Close()

	if res.StatusCode != 200 {
		return nil, errors.Errorf("web3 GetUser error -> request %s -- response %s", res.Request.URL, res.Status)
	}

	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		return nil, errors.Errorf("web3 GetUser error -> %s", err)
	}

	return body, nil
}

// Get token balance
func (w *Web3) GetUserTokenBalance(userAddress common.Address, tokenAddress common.Address) (*big.Int, error) {
	// TODO: wrap a generic token in the token module, not the giftable one
	instance, err := token.NewToken(tokenAddress, w.client)
	if err != nil {
		return nil, fmt.Errorf("cannot instantiate the token contract: %v", err)
	}

	bal, err := instance.BalanceOf(&bind.CallOpts{}, userAddress)

	if err != nil {
		return nil, fmt.Errorf("cannot get balance: %v", err)
	}
	return bal, nil
}

// Fetch token details from the contract
func (w *Web3) GetTokenDetails(tokenAddress *common.Address) (*TokenDetails, error) {
	var err error
	instance, err := token.NewToken(*tokenAddress, w.client)
	if err != nil {
		return nil, fmt.Errorf("cannot instantiate the token contract: %v", err)
	}

	name, err := instance.Name(&bind.CallOpts{})
	if err != nil {
		return nil, fmt.Errorf("cannot get token contract Name: %v", err)
	}

	symbol, err := instance.Symbol(&bind.CallOpts{})
	if err != nil {
		return nil, fmt.Errorf("cannot get token contract Symbol: %v", err)
	}

	tokenDetails := &TokenDetails{
		Name:   name,
		Symbol: symbol,
	}

	return tokenDetails, nil
}

// DEPRECATED please use cic/meta/meta MetaGetPerson
// Get Account Type from cic meta service
func (w *Web3) GetAccountType(s BlockchainAddress) (string, error) {
	// Doing this validation here to guard against bad data in this unhelpful BlockchainAddress type
	isValidAddress := common.IsHexAddress(s.String())
	if !isValidAddress {
		return "", fmt.Errorf("cannot convert %s to valid address", s.String())
	}
	address := common.HexToAddress(s.String())
	key := meta.GenerateMetaKeyCustom(&address)

	url := fmt.Sprintf("%s/%s", config.Conf.MetaServiceUrl, key)

	res, err := w.getter.Get(url)
	if err != nil {
		return "", errors.Errorf("web3 GetAccountType error -> %s", err)
	}
	defer res.Body.Close()

	// TODO: struct is not sure, need to be reviewed
	var accountType UserAccountType

	switch res.StatusCode {
	case 200:
		body, readErr := ioutil.ReadAll(res.Body)
		if readErr != nil {
			return "", err
		}

		err = json.Unmarshal(body, &accountType)
		if err != nil {
			return "", err
		}
	case 400:
		return "", errors.New("bad request")
	case 404:
		return "", errors.New("not found")
	}

	return accountType.AccountType, nil
}

// DEPRECATED please use cic/meta/meta GetMetaCustom
func (w *Web3) GetPreferences(s BlockchainAddress) (string, bool, error) {
	isValidAddress := common.IsHexAddress(s.String())
	if !isValidAddress {
		return "", false, fmt.Errorf("cannot convert %s to valid address", s.String())
	}
	address := common.HexToAddress(s.String())
	key := meta.GenerateMetaKeyPreferences(&address)

	url := fmt.Sprintf("%s/%s", config.Conf.MetaServiceUrl, key)

	res, err := w.getter.Get(url)
	if err != nil {
		return "", false, errors.Errorf("web3 getPreferences error: %s", err)
	}
	defer res.Body.Close()

	// TODO: struct is not sure, need to be reviewed
	var preferences UserPreferences

	switch res.StatusCode {
	case 200:
		body, readErr := ioutil.ReadAll(res.Body)
		if readErr != nil {
			return "", false, err
		}

		err = json.Unmarshal(body, &preferences)
		if err != nil {
			return "", false, err
		}
	case 400:
		return "", false, errors.New("bad request")
	case 404:
		return "", false, errors.New("not found")
	}

	return preferences.PreferredLanguage, preferences.IsMarketEnabled, nil
}
