package meta

import (
	"log"
	"testing"

	"github.com/ethereum/go-ethereum/common"
)

var logg *log.Logger = log.Default()

func TestGenerateMetaKeyCustom(t *testing.T) {
	testAddress := "0x68357742958b88c378847Bac9577Ed59e11D3f6F"
	a := common.HexToAddress(testAddress)
	got := GenerateMetaKeyCustom(&a)
	expected := "c86bcb005380ed852f63b5032bba03fb069ff181f0dfd1d91443f8389bd508f6"
	if got != expected {
		t.Errorf("got: %v expected %v", got, expected)
	}
}

func TestGenerateMetaKeyPreferences(t *testing.T) {
	testAddress := "0x68357742958b88c378847Bac9577Ed59e11D3f6F"
	a := common.HexToAddress(testAddress)
	got := GenerateMetaKeyPreferences(&a)
	expected := "a73ec054d8b9a54c04fd93b88eb8a305772fe8eaef5847881df2bf33fb67c860"
	if got != expected {
		t.Errorf("got: %v expected: %v", got, expected)
	}
}
