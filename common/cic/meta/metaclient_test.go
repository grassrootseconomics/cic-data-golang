package meta

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"strconv"
	"testing"

	"github.com/ethereum/go-ethereum/common"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/restclient"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/util/mocks"
)

func init() {
	MetaUrl = "http://mock-meta:8080"
}

func TestMetaClientMockResponses(t *testing.T) {

	metaClient, _ := NewMetaClient("http://mock-meta.net")

	t.Run("check that GetMetaPerson parses a mock response", func(t *testing.T) {
		addressString := "0x68357742958b88c378847Bac9577Ed59e11D3f6F"
		address := common.HexToAddress(addressString)

		metaResponseJson := "{\"date_registered\": 1619897179, \"gender\": \"male\", \"identities\": {\"evm\": {\"bloxberg\": {\"bloxberg:8996\": [\"12b56336ce14409c66da142c3ecfd98fe66c5d2c\"]}}}, \"location\": {\"area_name\": \"mnarani\", \"latitude\": -19.579414599126977, \"longitude\": 108.20844753098925}, \"products\": [\"teacher\"], \"vcard\": \"QkVHSU46VkNBUkQNClZFUlNJT046My4wDQpFTUFJTDpob2R6aWN0aW1vdGVqQGVtYWlsLnNpDQpGTjpIYW5zLUguXCwgUG90dGVyDQpOOlBvdHRlcjtIYW5zLUguOzs7DQpURUw7VFlQPUNFTEw6KzI1NDQ0Nzc0NTIwMw0KRU5EOlZDQVJEDQo=\"}"
		r := ioutil.NopCloser(bytes.NewReader([]byte(metaResponseJson)))

		restclient.Client = &mocks.MockClient{}
		mocks.GetDoFunc = func(*http.Request) (*http.Response, error) {
			return &http.Response{
				StatusCode: 200,
				Body:       r,
			}, nil
		}

		person, err := metaClient.GetMetaPerson(&address, http.Header{})
		if err != nil {
			t.Errorf("%s", err.Message)
		}

		if person.Gender != "male" {
			t.Errorf("wanted male got %s", person.Gender)
		}
	})

	t.Run("check that GetMetaCustom parses a mock reponse", func(t *testing.T) {
		addressString := "0x68357742958b88c378847Bac9577Ed59e11D3f6F"
		address := common.HexToAddress(addressString)

		metaResponseJson := "{\"account_type\": \"individual\"}"
		r := ioutil.NopCloser(bytes.NewReader([]byte(metaResponseJson)))
		restclient.Client = &mocks.MockClient{}
		mocks.GetDoFunc = func(*http.Request) (*http.Response, error) {
			return &http.Response{
				StatusCode: 200,
				Body:       r,
			}, nil
		}

		custom, err := metaClient.GetMetaCustom(&address, http.Header{})
		if err != nil {
			t.Errorf("%s", err.Message)
		}

		if custom.AccountType != "individual" {
			t.Errorf("expected account_type individual got: %s", custom.AccountType)
		}
	})

	t.Run("check that GetMetaPreferences parses a mock reponse", func(t *testing.T) {
		addressString := "0x68357742958b88c378847Bac9577Ed59e11D3f6F"
		address := common.HexToAddress(addressString)

		metaResponseJson := "{\"preferred_language\": \"english\", \"is_market_enabled\": true}"
		r := ioutil.NopCloser(bytes.NewReader([]byte(metaResponseJson)))
		restclient.Client = &mocks.MockClient{}
		mocks.GetDoFunc = func(*http.Request) (*http.Response, error) {
			return &http.Response{
				StatusCode: 200,
				Body:       r,
			}, nil
		}

		preferences, err := metaClient.GetMetaPreferences(&address, http.Header{})
		if err != nil {
			t.Errorf("%s", err.Message)
		}

		if preferences.PreferredLanguage != "english" {
			t.Errorf("expected english got: %s", preferences.PreferredLanguage)
		}

		if preferences.IsMarketEnabled != true {
			t.Errorf("expected true got: %s", strconv.FormatBool(preferences.IsMarketEnabled))
		}
	})
}
