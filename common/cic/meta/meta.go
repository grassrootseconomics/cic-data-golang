package meta

import (
	"strings"

	"github.com/ethereum/go-ethereum/common"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/util"
)


// TODO get metas response format
// ErrorResponse describes the errors
type ErrorResponse struct {
	StatusCode int     `json:"status_code"`
	Message    string  `json:"message"`
	Errors     []Error `json:"errors"`
}

// Error describes the error
type Error struct {
	Resource string `json:"resource"`
	Code     string `json:"code"`
	Field    string `json:"field"`
	Message  string `json:"message"`
}

// PersonResponse
type MetaResponsePerson struct {
	DateRegistered int         `json:"date_registered"`
	Gender         string      `json:"gender"`
	Identity       Identities  `json:"identities"`
	Location       Location    `json:"location"`
	Products       []string    `json:"products"`
	DateOfBirth    DateOfBirth `json:"date_of_birth"`
}

type MetaResponseCustom struct {
	AccountType string `json:"account_type"`
}

type MetaResponsePreferenes struct {
	PreferredLanguage string `json:"preferred_language"`
	IsMarketEnabled string `json:"is_market_enabled"`
}

type Identities struct {
	Evm Evm `json:"evm"`
}

type Evm struct {
	Bloxberg []string `json:"bloxberg:8996"`
}

type Location struct {
	AreaName string `json:"area_name"`
}

type DateOfBirth struct {
	Year  int `json:"year"`
	Month int `json:"month"`
	Day   int `json:"day"`
}

type MetaResponsePreferences struct {
	PreferredLanguage string `json:"preferred_language"`
	IsMarketEnabled   bool   `json:"is_market_enabled"`
}

// ----

func GenerateMetaKeyCustom(address *common.Address) string {
	metaType := ":cic.custom"
	return util.MergeSha256Key(address.Bytes(), []byte(metaType))
}

func GenerateMetaKeyPreferences(address *common.Address) string {
	// preferences need to be trimmed of 0x prefix before hashing with type
	noPrefixAddress := strings.TrimLeft(address.Hex(), "0x")
	metaType := ":cic.preferences"
	return util.MergeSha256Key([]byte(noPrefixAddress), []byte(metaType))
}

func GenerateMetaKeyPerson(address *common.Address) string {
	metaType := ":cic.person"
	return util.MergeSha256Key(address.Bytes(), []byte(metaType))
}
