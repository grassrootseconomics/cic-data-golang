package meta

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/ethereum/go-ethereum/common"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/restclient"
)

var MetaUrl string

type MetaClient struct {
	url string
}

func NewMetaClient(url string) (*MetaClient, error) {
	if url == "" {
		return nil, errors.New("invalid cic meta url")
	}
	return &MetaClient{
		url,
	}, nil
}

func (m *MetaClient) GetMetaPerson(address *common.Address, headers http.Header) (*MetaResponsePerson, *ErrorResponse) {
	key := GenerateMetaKeyPerson(address)
	url := fmt.Sprintf("%s/%s", m.url, key)

	res, err := restclient.Get(url, http.Header{})
	if err != nil {
		log.Printf("Error fetching cic.person from meta %s", err)
		return nil, &ErrorResponse{
			StatusCode: http.StatusInternalServerError,
			Message:    err.Error(),
		}
	}

	bytes, err := ioutil.ReadAll(res.Body)
	defer res.Body.Close()

	if err != nil {
		return nil, &ErrorResponse{
			StatusCode: http.StatusInternalServerError,
			Message:    "Invalid response body",
		}
	}

	if res.StatusCode > 299 {
		var errResponse ErrorResponse
		if err := json.Unmarshal(bytes, &errResponse); err != nil {
			return nil, &ErrorResponse{
				StatusCode: res.StatusCode,
				Message:    "Invalid response body",
			}
		}
		// TODO not sure can reach this need to research meta error response
		return nil, &errResponse
	}

	var result MetaResponsePerson
	if err := json.Unmarshal(bytes, &result); err != nil {
		return nil, &ErrorResponse{
			StatusCode: res.StatusCode,
			Message:    "Error trying to decode successful get cic.person request",
		}
	}
	return &result, nil
}

// get digest from cic.custom
// returns account type
func (m *MetaClient) GetMetaCustom(address *common.Address, headers http.Header) (*MetaResponseCustom, *ErrorResponse) {
	key := GenerateMetaKeyCustom(address)
	url := fmt.Sprintf("%s/%s", m.url, key)

	res, err := restclient.Get(url, http.Header{})
	if err != nil {
		log.Printf("Error fetching cic.custom from meta %s", err)
		return nil, &ErrorResponse{
			StatusCode: http.StatusInternalServerError,
			Message:    err.Error(),
		}
	}

	bytes, err := ioutil.ReadAll(res.Body)
	defer res.Body.Close()

	if err != nil {
		return nil, &ErrorResponse{
			StatusCode: http.StatusInternalServerError,
			Message:    "Invalid response body",
		}
	}

	if res.StatusCode > 299 {
		var errResponse ErrorResponse
		if err := json.Unmarshal(bytes, &errResponse); err != nil {
			return nil, &ErrorResponse{
				StatusCode: res.StatusCode,
				Message:    "Invalid response body",
			}
		}
		// TODO not sure can reach this need to research meta error response
		return nil, &errResponse
	}

	var result MetaResponseCustom
	if err := json.Unmarshal(bytes, &result); err != nil {
		return nil, &ErrorResponse{
			StatusCode: res.StatusCode,
			Message:    "Error trying to decode successful get cic.meta request",
		}
	}
	return &result, nil
}

func (m *MetaClient) GetMetaPreferences(address *common.Address, headers http.Header) (*MetaResponsePreferences, *ErrorResponse) {
	key := GenerateMetaKeyPreferences(address)
	url := fmt.Sprintf("%s/%s", m.url, key)

	res, err := restclient.Get(url, http.Header{})
	if err != nil {
		log.Printf("Error fetching cic.Preferences from meta %s", err)
		return nil, &ErrorResponse{
			StatusCode: http.StatusInternalServerError,
			Message:    err.Error(),
		}
	}

	bytes, err := ioutil.ReadAll(res.Body)
	defer res.Body.Close()

	if err != nil {
		return nil, &ErrorResponse{
			StatusCode: http.StatusInternalServerError,
			Message:    "Invalid response body",
		}
	}

	if res.StatusCode > 299 {
		var errResponse ErrorResponse
		if err := json.Unmarshal(bytes, &errResponse); err != nil {
			return nil, &ErrorResponse{
				StatusCode: res.StatusCode,
				Message:    "Invalid response body",
			}
		}
		// TODO not sure can reach this need to research meta error response
		return nil, &errResponse
	}

	var result MetaResponsePreferences
	if err := json.Unmarshal(bytes, &result); err != nil {
		return nil, &ErrorResponse{
			StatusCode: res.StatusCode,
			Message:    "Error trying to decode successful get cic.meta request",
		}
	}
	return &result, nil
}
