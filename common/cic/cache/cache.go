package ciccache

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"gitlab.com/grassrootseconomics/cic-data-golang/common/bigint"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/restclient"
)

type CacheDataResponse struct {
	Data []*CacheTransactionResponse `json:"data"`
}

type CacheTransactionResponse struct {
	BlockNumber      int            `json:"block_number"`
	TxHash           string         `json:"tx_hash"`
	DateBlock        float64        `json:"date_block"`
	Sender           string         `json:"sender"`
	Recipient        string         `json:"recipient"`
	FromValue        *bigint.Bigint `json:"from_value"`
	ToValue          *bigint.Bigint `json:"to_value"`
	SourceToken      string         `json:"source_token"`
	DestinationToken string         `json:"destination_token"`
	Success          bool           `json:"success"`
	TxType           string         `json:"tx_type"`
}

// ErrorResponse describes the errors
type ErrorResponse struct {
	StatusCode int     `json:"status_code"`
	Message    string  `json:"message"`
	Errors     []Error `json:"errors"`
}

// Error describes the error
type Error struct {
	Resource string `json:"resource"`
	Code     string `json:"code"`
	Field    string `json:"field"`
	Message  string `json:"message"`
}

type CacheClient struct {
	url string
}

func NewCacheClient(url string) (*CacheClient, error) {
	if url == "" {
		return nil, errors.New("invalid cic cache url")
	}
	return &CacheClient{
		url,
	}, nil
}

// Get transaction from block number start to blocknumber end
func (c *CacheClient) GetTransactions(start int, end int) ([]*CacheTransactionResponse, *ErrorResponse) {
	url := fmt.Sprintf("%s/txa/%d/%d", c.url, start, end)
	headers := http.Header{}
	headers.Set("X-CIC-CACHE-MODE", "all")
	res, err := restclient.Get(url, headers)
	if err != nil {
		log.Printf("Error fetching transaction from cic cache service %s", err)
		return nil, &ErrorResponse{
			StatusCode: http.StatusInternalServerError,
			Message:    err.Error(),
		}
	}

	bytes, err := ioutil.ReadAll(res.Body)
	defer res.Body.Close()

	if err != nil {
		return nil, &ErrorResponse{
			StatusCode: http.StatusInternalServerError,
			Message:    "Ivalid response body",
		}
	}

	if res.StatusCode > 299 {
		var errResponse ErrorResponse
		if err := json.Unmarshal(bytes, &errResponse); err != nil {
			return nil, &ErrorResponse{
				StatusCode: res.StatusCode,
				Message:    "Invalid response body",
			}
		}
		return nil, &errResponse
	}

	var result CacheDataResponse
	if err := json.Unmarshal(bytes, &result); err != nil {
		return nil, &ErrorResponse{
			StatusCode: res.StatusCode,
			Message:    "Error trying to decode successfull get cache transaction request",
		}
	}
	return result.Data, nil
}
