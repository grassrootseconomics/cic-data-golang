package ciccache

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"testing"
	"time"

	"gitlab.com/grassrootseconomics/cic-data-golang/common/restclient"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/util/mocks"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/util/testutils"
)

// generate transaction for a  single token type
func generateFakeTransactionResponse(blockNumber int, token string) string {
	if blockNumber < 1 {
		blockNumber = 1
	}

	transferAmmount := rand.Intn(10000) + 1

	txHash := testutils.GenerateEthAddress().Hex()
	dateBlock := time.Now().Unix()
	sender := testutils.GenerateEthAddress().Hex()
	recipient := testutils.GenerateEthAddress().Hex()
	from_value := transferAmmount
	to_value := transferAmmount
	source_token := token
	destination_token := token
	success := "true"
	tx_type := "individual"

	return fmt.Sprintf("{\"block_number\": %d,"+
		"\"tx_hash\": \"%s\","+
		"\"date_block\": %d,"+
		"\"sender\": \"%s\","+
		"\"recipient\": \"%s\","+
		"\"from_value\": %d,"+
		"\"to_value\": %d,"+
		"\"source_token\": \"%s\","+
		"\"destination_token\": \"%s\","+
		"\"success\": %s,"+
		"\"tx_type\": \"%s\"}",
		blockNumber,
		txHash,
		dateBlock,
		sender,
		recipient,
		from_value,
		to_value,
		source_token,
		destination_token,
		success,
		tx_type)
}

func TestGenerateFakeTransactionReponse(t *testing.T) {
	r := generateFakeTransactionResponse(1, "BEEPO")
	var result CacheTransactionResponse
	if err := json.Unmarshal([]byte(r), &result); err != nil {
		t.Errorf("your fake transaction is bad \n%v\n%s", r, err)
	}
}

func TestGetTransactions(t *testing.T) {
	transactionResponseString := generateFakeTransactionResponse(1, "BEEPO")
	transactionResponseDataString := fmt.Sprintf("{\"data\": [%s]}", transactionResponseString)

	r := ioutil.NopCloser(bytes.NewReader([]byte(transactionResponseDataString)))

	restclient.Client = &mocks.MockClient{}
	mocks.GetDoFunc = func(*http.Request) (*http.Response, error) {
		return &http.Response{
			StatusCode: 200,
			Body:       r,
		}, nil
	}

	cacheClient, _ := NewCacheClient("http://mock-cache:8080")

	transactions, err := cacheClient.GetTransactions(0, 100)
	if err != nil {
		t.Errorf("%s", err.Message)
	}
	if transactions[0].SourceToken != "BEEPO" {
		fmt.Printf("No please not beepo 🔪")
	}
}
