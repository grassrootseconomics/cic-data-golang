package bigint

import (
	"database/sql"
	"database/sql/driver"
	"fmt"
	"math/big"
)

// Supports writing to the database with text format https://github.com/go-gorm/gorm/issues/4600
type Bigint big.Int
// TODO Please see https://pkg.go.dev/github.com/shopspring/decimal
// as a replacement for this type

func NewBigint(x *big.Int) *Bigint {
	return (*Bigint)(x)
}

func FromInt64(x int64) *Bigint {
	return new(Bigint).FromBigInt(big.NewInt(x))
}

func FromString(x string) (*Bigint, error) {
	if x == "" {
		return FromInt64(0), nil
	}
	a := big.NewInt(0)
	b, ok := a.SetString(x, 10)

	if !ok {
		return nil, fmt.Errorf("cannot create Bigint from string")
	}

	return NewBigint(b), nil
}

func (b *Bigint) Value() (driver.Value, error) {
	// to fix handle nil values
	if b == nil {
		return "", nil
	}
	return (*big.Int)(b).String(), nil
}

func (b *Bigint) Scan(value interface{}) error {

	var i sql.NullString
	if err := i.Scan(value); err != nil {
		return err
	}

	// trying to patch the scanner to read the db value if the value is nul on write
	// TODO bug if values are not set on Bigint then it write the string '<nil>' to the database
	// OR use 	"github.com/shopspring/decimal" which the gorm team recommends
	if i.String == "<nil>" || len(i.String) == 0 {
		if _, ok := (*big.Int)(b).SetString("0", 10); ok {
			return nil
		}
	}

	if _, ok := (*big.Int)(b).SetString(i.String, 10); ok {
		return nil
	} else {
		fmt.Printf("DEBUG value of string is %+v", i)
	}

	return fmt.Errorf("converting type %T (value %v)into Bigint", value, value)
}

func (b *Bigint) toBigInt() *big.Int {
	return (*big.Int)(b)
}

func (b *Bigint) Sub(x *Bigint) *Bigint {
	return (*Bigint)(big.NewInt(0).Sub(b.toBigInt(), x.toBigInt()))
}

func (b *Bigint) Add(x *Bigint) *Bigint {
	return (*Bigint)(big.NewInt(0).Add(b.toBigInt(), x.toBigInt()))
}

func (b *Bigint) Mul(x *Bigint) *Bigint {
	return (*Bigint)(big.NewInt(0).Mul(b.toBigInt(), x.toBigInt()))
}

func (b *Bigint) Div(x *Bigint) *Bigint {
	return (*Bigint)(big.NewInt(0).Div(b.toBigInt(), x.toBigInt()))
}

func (b *Bigint) Neg() *Bigint {
	return (*Bigint)(big.NewInt(0).Neg(b.toBigInt()))
}

func (b *Bigint) ToUInt64() uint64 {
	return b.toBigInt().Uint64()
}

func (b *Bigint) ToInt64() int64 {
	return b.toBigInt().Int64()
}

// same as NewBigint()
func (b *Bigint) FromBigInt(x *big.Int) *Bigint {
	return (*Bigint)(x)
}

func (b *Bigint) String() string {
	return b.toBigInt().String()
}

func (b *Bigint) Cmp(target *Bigint) Cmp {
	return &cmp{r: b.toBigInt().Cmp(target.toBigInt())}
}

func (b *Bigint) Abs() *Bigint {
	return (*Bigint)(new(big.Int).Abs(b.toBigInt()))
}

func (b *Bigint) MarshalJSON() ([]byte, error) {
	return []byte(b.String()), nil
}

func (b *Bigint) UnmarshalJSON(p []byte) error {
	val, err := FromString(string(p))
	if err != nil {
		return err
	}

	*b = *val

	return nil
}
