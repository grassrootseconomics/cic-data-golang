CREATE USER test WITH PASSWORD 'fralala' CREATEDB;
CREATE DATABASE "cic_data_test";
GRANT ALL PRIVILEGES
ON DATABASE "cic_data_test"
TO test;
